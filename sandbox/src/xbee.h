#ifndef XBEE_H
#define XBEE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <fcntl.h>
//this is the XBEE maximum
#define BUFFER_LENGTH 100

struct xbee_t {
	int port;
	speed_t baudRate;
	const char *devName;
	const int bufflen = BUFFER_LENGTH;
	char wbuff[BUFFER_LENGTH];
	char rbuff[BUFFER_LENGTH];
	int wbytes;
	int rbytes;

	struct termios attr; //Define here or in xbee init?
};

void xbeePrintReadBuffer();
int xbeeWriteStr(char * str);
int xbeeInit();
int xbeeRefresh();

#endif //XBEE_H
