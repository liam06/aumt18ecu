#include "xbee.h"
#include <termios.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/unistd.h>
#include <getopt.h>


#define debug 1

xbee_t xbee;

int xbeeInit(){
//	struct termios stdin_tio;
//	struct termios stdin_tio_org;


	xbee.devName = "/dev/ttyS1";

	xbee.baudRate = B115200;

	xbee.port = -1;


	if (( xbee.port = open( xbee.devName, O_RDWR )) < 0) {

		printf("Unable to open serial port\n");
		return -1;

	}

	// Serial port should be open

	// Now removing non-blocking behaviour
	//fcntl (xbee.port, F_SETFL, fcntl(xbee.port, F_GETFL) & ~O_NONBLOCK);

	if (tcgetattr(xbee.port, &xbee.attr) <0) {
		printf("Call to tcgetattr failed\n");
		return -1;
	}

	//cfmakeraw( &xbee.attr);


	bzero(&xbee.attr, sizeof(xbee.attr)); // setting locations to zero

	//IGNPAR 	- Ignore btyes with parity erros
    xbee.attr.c_cflag = xbee.baudRate | CRTSCTS | CS8 | CLOCAL | CREAD;
    xbee.attr.c_iflag = IGNPAR;
    xbee.attr.c_oflag = 0;


    xbee.attr.c_cc[VTIME] = 0;
    xbee.attr.c_cc[VMIN] = 0;
    /* end of edit */


    xbee.attr.c_lflag = 0; //non-canonical, no echo.


    tcflush(xbee.port, TCIFLUSH);

    if (tcsetattr(xbee.port,TCSANOW,&xbee.attr) < 0) {
		printf("Call to tcsetattr failed");
		return -1;
	}

	return 0;
}

int xbeeWriteStr(char * str){
	memset(&xbee.wbuff, 0, xbee.bufflen);
	xbee.wbytes = strlen(str);
	strcpy(xbee.wbuff, str);
	return 0;
}

void xbeePrintReadBuffer() {
	// work out how to print character by character

	/*
	if(xbee.rbytes > 0) {
		for (int i=0; i<xbee.rbytes; i++) {
			printf("%c", &xbee.rbuff[i]);
		}

		xbee.rbytes=0;
	}
	*/

	if (xbee.rbytes > 0){
		printf("%s\n", (char*)xbee.rbuff);
		memset(xbee.rbuff, 0, xbee.rbytes);
		xbee.rbytes = 0;
	}
}



int xbeeRefresh() {
	//check in send buffer
	//send to xbee


	{ // write to xbee


		int bytesWritten;

		if (xbee.wbytes > 0) {
			bytesWritten = write(xbee.port, &xbee.wbuff, xbee.wbytes);
			memset(&xbee.wbuff, 0, xbee.wbytes);

			if (bytesWritten < -1){
				printf("Write to Xbee failed\n");
				return -1;
			} else if (bytesWritten != xbee.wbytes) {
				printf("Buffer was incompletely written");
			}

			xbee.wbytes =0;


		}


	}
	{ // read from xbee
		xbee.rbytes = read(xbee.port, &xbee.rbuff, xbee.bufflen);
		xbeePrintReadBuffer();
		//memset(xbee.rbuff, 0, xbee.bufflen);

		/*
		if (xbee.rbytes < 0){
			printf("Serial read failed\n");
			xbee.rbytes = 0;
			return -1;
		} else if (debug){
			//printf("RCV: %c\n",ch);
		}

		*/
	}
	return 0;
}
