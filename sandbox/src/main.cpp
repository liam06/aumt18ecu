//============================================================================
// Name        : main.cpp
// Author  	   :
// Version     :
// Copyright   : Your copyright notice
// Description : Execution Start for EVSYS in C++
//============================================================================

#include <iostream>
#include <stdio.h>
#include <termios.h> // Serial Library
#include <unistd.h>
#include <fcntl.h>
#include "xbee.h"


int main() {
	printf("Start EVSYS V0.1\n");
	bool quit = false;
	if (xbeeInit() < 0) {
		quit = true;
	}
	int count = 0;
	while (!quit) {

		usleep(1e3);


		if (xbeeRefresh() < 0){
			quit = true;
			printf("refresh error\n");
			break;
		}

		count++;
		if (count % 1000 == 0){
			char str[100];
			sprintf(str,"XBEESENDPACKET[%i]\n",count/1000 );
			xbeeWriteStr(str);
		}



	}

	return 0;
}
