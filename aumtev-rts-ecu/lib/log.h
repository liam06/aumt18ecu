#ifndef _LOG_H
#define _LOG_H

#include <stdbool.h>
#include <stdio.h>

#include "queue.h"

typedef struct {
    FILE *f;  // file handle
    unsigned int rows;
    unsigned int cols;
    size_t cellSize;
    queueInfo_t logBuffer;
} logInfo_t;


bool logInit(logInfo_t* logInfo, void **logBuffer, char* filename, unsigned int rows, unsigned int cols, size_t cellSize);
bool logInsertRow(logInfo_t* logInfo, void *dataRow);
bool logPopRow(logInfo_t* logInfo, void *dataRowOut);

#endif