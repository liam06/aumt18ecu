#include "bitsplit.h"

#include <stdint.h>


void splitIntToBytes(uint32_t val, uint8_t *byteArrayOut, uint8_t len, endianness_t endianness) 
{
	int i;

	if (endianness == endianBig)
	{
		for (i = 0; i < len; i++) {
			byteArrayOut[len-i-1] = (uint8_t)val;
			val >>= 8;
		}
	} 
	else if (endianness == endianLittle)
	{
		for (i = 0; i < len; i++) {
			byteArrayOut[i] = (uint8_t)val;
			val >>= 8;
		}
	}
}

uint32_t combineBytesToInt(uint8_t *byteArray, uint8_t len, endianness_t endianness) 
{
	uint32_t val = 0;
	int i;
	
	if (endianness == endianBig)
	{
		for (i = 0; i < len; i++) {
			val <<= 8;
			val |= byteArray[i];
		}
	}
	else if (endianness == endianLittle)
	{
		for (i = 0; i < len; i++) {
			val <<= 8;
			val |= byteArray[len-i-1];
		}
	}

	return val;
}
