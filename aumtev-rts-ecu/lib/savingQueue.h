#ifndef _SAVINGQUEUE_H
#define _SAVINGQUEUE_H

#include <stdbool.h>
#include <stdio.h>

#include "queue.h"

typedef struct {
    FILE *f;  // file handle
    queueInfo_t queue;
} savingQueueInfo_t;


bool savingQueueInit(savingQueueInfo_t* savingQueueInfo, void **buffer, char* filename, unsigned int bufferSize, size_t cellSize);
bool savingQueueInsert(savingQueueInfo_t* savingQueueInfo, void *data);
bool savingQueuePop(savingQueueInfo_t* savingQueueInfo, void *dataOut);

#endif