#include "log.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include "lib/queue.h"
#include "system/tasks.h"

void *logTask(void* argp);

bool logInit(logInfo_t* logInfo, void **logBuffer, char* filename, unsigned int rows, unsigned int cols, size_t cellSize)
{
	logInfo->rows = rows;
	logInfo->cols = cols;
	logInfo->cellSize = cellSize;
	if (!queueInit(&logInfo->logBuffer, logBuffer, rows*cols, cols*cellSize))
	{
		printf("error\n");
		return false;
	}
	
	// open file handle
	logInfo->f = fopen(filename, "wb");
	if (logInfo->f == NULL)
	{
		printf("couldn't open file\n");
		return false;
	}
	
	// create dumping thread
	pthread_t threadTask_id;
	if (taskCreateRR(&threadTask_id, logTask, (void*)logInfo, 1) == -1)
	{
		printf("ERROR: lib/log: Could not set up background thread\n");
		return false;
	}
	
	return true;
}

// insert to log
bool logInsertRow(logInfo_t* logInfo, void *data)
{
	return queueInsert(&(logInfo->logBuffer), data);
}

// dumping task
bool logPopRow(logInfo_t* logInfo, void *dataOut)
{
	return queuePop(&logInfo->logBuffer, (void*)dataOut);
}

void *logTask(void* argp)
{
	logInfo_t *log = (logInfo_t*)argp;
	while (true)
	{
		if (queueLen(&(log->logBuffer)) == 0)
		{
			usleep(5e3);  // 5ms
			continue;
		}
		uint8_t dataRow[log->cols*log->cellSize];      // storage of bytes - little endian? (LSB is first)
		logPopRow(log, (void*)dataRow);
		// dataRow is size log->cols*log->cellSize
		unsigned int dataRowSize = log->cols*log->cellSize;
		fwrite(dataRow, 1, dataRowSize, log->f);
	}

	return NULL;
}