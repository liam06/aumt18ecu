#ifndef _STATES_H
#define _STATES_H

typedef unsigned short int state_t;

#define STATE_INIT (state_t)0
#define STATE_RUN (state_t)1
#define STATE_ERROR (state_t)2

#endif
