#include "queue.h"

#include <stdbool.h>
#include <pthread.h>
#include <string.h>


bool queueInit(queueInfo_t* queueInfo, void **buffer, unsigned int bufferSize, size_t elementSize)
{
    queueInfo->bufferFirst = 0;
    queueInfo->bufferLast = 0;
    queueInfo->bufferCount = 0;
    queueInfo->bufferSize = bufferSize;
    queueInfo->buffer = buffer;
    queueInfo->elementSize = elementSize;

    if (pthread_mutex_init(&queueInfo->bufferMutex, NULL) != 0) 
	{
		return false; // couldn't create mutex
	}

    return true;
}

bool queueInsert(queueInfo_t* queueInfo, void *value)
{
    // don't let another thread read buffer now
    pthread_mutex_lock(&queueInfo->bufferMutex); 

    // place value in buffer
    if (queueInfo->bufferCount == queueInfo->bufferSize)
    {
        pthread_mutex_unlock(&queueInfo->bufferMutex); 
        return false;
    }
	
	void* newElement = (((char*)queueInfo->buffer) + queueInfo->bufferLast*queueInfo->elementSize); // cast to char* because char is 1 byte wide
	memcpy(newElement, value, queueInfo->elementSize);
	

    // done with shared buffer
    pthread_mutex_unlock(&queueInfo->bufferMutex); 

    queueInfo->bufferLast = (queueInfo->bufferLast + 1) % queueInfo->bufferSize;
    queueInfo->bufferCount++;

    return true;
}

bool queuePop(queueInfo_t* queueInfo, void *value)
{
    // don't allow the receiver to write to buffer for now
    pthread_mutex_lock(&queueInfo->bufferMutex);  

	// Retreieve first element of buffer:
	if (queueInfo->bufferCount == 0) 
	{
		pthread_mutex_unlock(&queueInfo->bufferMutex);
		return false;
	}

    // "return" the value
    void* element = (((char*)queueInfo->buffer) + queueInfo->bufferFirst*queueInfo->elementSize); // cast to char* because char is 1 byte wide
	memcpy(value, element, queueInfo->elementSize);
	
    queueInfo->bufferFirst = (queueInfo->bufferFirst + 1) % queueInfo->bufferSize;
    queueInfo->bufferCount--;

    // done reading shared resource
	pthread_mutex_unlock(&queueInfo->bufferMutex);  


    return true;
}

unsigned int queueLen(queueInfo_t* queueInfo)
{
	return queueInfo->bufferCount;
}