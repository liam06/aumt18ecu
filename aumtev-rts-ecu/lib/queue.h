#ifndef _QUEUE_H
#define _QUEUE_H

#include <stdbool.h>
#include <pthread.h>

typedef struct {
    unsigned int bufferFirst;
    unsigned int bufferLast;
    unsigned int bufferCount;
    unsigned int bufferSize;
    size_t elementSize;
    void **buffer;
    pthread_mutex_t bufferMutex;
} queueInfo_t;


bool queueInit(queueInfo_t* queueInfo, void **buffer, unsigned int bufferSize, size_t elementSize);
bool queueInsert(queueInfo_t* queueInfo, void *value);
bool queuePop(queueInfo_t* queueInfo, void *value); // returns false if fail. Passes value out through *value
unsigned int queueLen(queueInfo_t* queueInfo);

#endif