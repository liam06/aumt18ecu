#include "savingQueue.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include "lib/queue.h"
#include "system/tasks.h"

void *savingQueueTask(void* argp);

bool savingQueueInit(savingQueueInfo_t* savingQueueInfo, void **queue, char* filename, unsigned int bufferSize, size_t cellSize)
{
	if (!queueInit(&savingQueueInfo->queue, queue, bufferSize, cellSize))
	{
		printf("ERROR: lib/savingQueue: Error initializing queue\n");
		return false;
	}
	
	// open file handle
	savingQueueInfo->f = fopen(filename, "ab");
	if (savingQueueInfo->f == NULL)
	{
		printf("ERROR: lib/savingQueue: couldn't open file\n");
		return false;
	}
	
	// create dumping thread
	pthread_t threadTask_id;
	if (taskCreateRR(&threadTask_id, savingQueueTask, (void*)savingQueueInfo, 1) == -1)
	{
		printf("ERROR: lib/savingQueue: Could not set up background thread\n");
		return false;
	}
	
	return true;
}

// insert to savingQueue
bool savingQueueInsert(savingQueueInfo_t* savingQueueInfo, void *data)
{
	return queueInsert(&(savingQueueInfo->queue), data);
}

// dumping task
bool savingQueuePop(savingQueueInfo_t* savingQueueInfo, void *dataOut)
{
	return queuePop(&savingQueueInfo->queue, dataOut);
}

void *savingQueueTask(void* argp)
{
	savingQueueInfo_t *savingQueue = (savingQueueInfo_t*)argp;
	while (true)
	{
		if (queueLen(&(savingQueue->queue)) == 0)
		{
			usleep(5e3);  // 5ms
			continue;
		}

		void* data[1024];  // TODO: big enough?
		savingQueuePop(savingQueue, data);

		unsigned int dataSize = savingQueue->queue.bufferSize;
		fwrite(data, dataSize, 1, savingQueue->f);
	}

	return NULL;
}