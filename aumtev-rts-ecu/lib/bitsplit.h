#ifndef _BITSPLIT_H
#define _BITSPLIT_H

#include <stdint.h>

typedef enum {
    endianLittle = 0,
    endianBig = 1
} endianness_t;

// Split a single integer into an array of bytes
void splitIntToBytes(uint32_t val, uint8_t *byteArrayOut, uint8_t len, endianness_t endianness);

// Combine an array of bytes into a single integer
uint32_t combineBytesToInt(uint8_t *byteArray, uint8_t len, endianness_t endianness);

#endif