#include "datalogModule.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "driverh/appsA/appsADriver.h"
#include "driverh/appsB/appsBDriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverh/bpps/bppsDriver.h"
#include "driverh/bpsA/bpsADriver.h"
#include "driverh/bpsB/bpsBDriver.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/wheelSpeed/wheelSpeedFrontDriver.h"
#include "driverh/wheelSpeed/wheelSpeedRearDriver.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "modules/operation/operationModule.h"

#include "lib/states.h"
#include "lib/queue.h"
#include "system/tasks.h"


// Actual log storage
// logInfo_t logInfo;
// #define DATALOG_ROWS 100   /* Number of rows in time buffer */
// #define DATALOG_COLS 100     /* Number of variables */
// uint32_t datalogBuffer[DATALOG_ROWS][DATALOG_COLS];

state_t datalogModuleState = STATE_INIT;


typedef struct {
    uint16_t appsASensorValueRaw;
    uint16_t appsBSensorValueRaw;
    uint16_t bpsASensorValueRaw;
    bool errorIMD;
    bool errorBMS;
    bool errorBSPD;
    bool errorPDOC;
    bool errorECU;
    bool driverConfirmButton;
    vehicleDriveState_t vehicleDriveState;
} saveDataEcuSensors_t;
typedef struct {
    struct timespec t;
    saveDataEcuSensors_t ecuSensors;
    inverterSensorsRaw_t inverterSensors;
    bmsSensorsRaw_t bmsSensors;
} saveData_t;


// Logger
queueInfo_t queue;
#define LOG_LENGTH 100
saveData_t logBuffer[LOG_LENGTH];

FILE* f;
void *datalogTask(void* argp);

state_t datalogModuleInit()
{
	printf("LOG: modules/datalog: Initializing data logging module\n");

    // init log
    if (!queueInit(&queue, (void*)logBuffer, LOG_LENGTH, sizeof(saveData_t)))
	{
		printf("ERROR: lib/savingQueue: Error initializing queue\n");
		return false;
	}
	
	// open file handle
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	char filename[64];
	sprintf(filename, "/root/logs/sensors__%04d-%02d-%02d__%02d-%02d-%02d.dat", 
				tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, 
				tm.tm_hour, tm.tm_min, tm.tm_sec);
	f = fopen(filename, "wb");
	if (f == NULL)
	{
		printf("ERROR: lib/savingQueue: couldn't open file\n");
		return false;
	}
	
	// create dumping thread
	pthread_t threadTask_id;
	if (taskCreateRR(&threadTask_id, datalogTask, NULL, 1) == -1)
	{
		printf("ERROR: lib/savingQueue: Could not set up background thread\n");
		return false;
	}

	// call init to simulink model
	datalogModuleSetState(STATE_RUN);
	return datalogModuleGetState();
}

state_t datalogModuleTick()
{
	// append data to the log
    saveData_t saveData;
    clock_gettime(CLOCK_MONOTONIC, &saveData.t);
    saveData.ecuSensors.appsASensorValueRaw = appsASensorValueRaw;
    saveData.ecuSensors.appsBSensorValueRaw = appsBSensorValueRaw;
    saveData.ecuSensors.bpsASensorValueRaw = bpsASensorValueRaw;
    saveData.ecuSensors.errorIMD = sysWarningStatus.errorIMD;
    saveData.ecuSensors.errorBMS = sysWarningStatus.errorBMS;
    saveData.ecuSensors.errorBSPD = sysWarningStatus.errorBSPD;
    saveData.ecuSensors.errorPDOC = sysWarningStatus.errorPDOC;
    saveData.ecuSensors.errorECU = sysWarningStatus.errorECU;

    saveData.inverterSensors = inverterSensorsRaw;
    saveData.bmsSensors = bmsSensorsRaw;

    queueInsert(&queue, (void*)&saveData);


	return datalogModuleGetState();
}

void *datalogTask(void* argp)
{
	while (true)
	{
		if (queueLen(&(queue)) == 0)
		{
			usleep(5e3);  // 5ms
			continue;
		}

		saveData_t data;  // TODO: big enough?
		queuePop(&queue, &data);

		fwrite(&data, sizeof(data), 1, f);
	}

	return NULL;
}

void datalogModuleSetState(const state_t state)
{
	datalogModuleState = state;
}

state_t datalogModuleGetState()
{
	return datalogModuleState;
}
