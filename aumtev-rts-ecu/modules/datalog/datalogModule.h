#ifndef _DATALOGMODULE_H
#define _DATALOGMODULE_H

#include <stdint.h>
#include "lib/states.h"

extern state_t datalogModuleInit();
extern state_t datalogModuleTick();

extern void datalogModuleSetState(const state_t state);
extern state_t datalogModuleGetState();

#endif
