#include "coolingModule.h"

#include <math.h>
#include <stdio.h>

#include "lib/states.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverl/gpio/gpio.h"

#include "coolingModuleCalibration.h"


// define connections
const pin_t *ACC1FanPin = &P8_13;
const pin_t *ACC2FanPin = &P8_15;
const pin_t *RadiatorPin = &P8_14;
const pin_t *WaterPumpPin = &P8_12;



state_t coolingModuleInit()
{
    printf("LOG: driverh/cooling: Initializing cooling driver\n");

    gpioWrite(ACC1FanPin, false);
    gpioWrite(ACC2FanPin, false);
    gpioWrite(RadiatorPin, false);
    gpioWrite(WaterPumpPin, false);

    return STATE_RUN;
}
int counter = 0;
state_t coolingModuleUpdate()
{    
    // water pump always on
    gpioWrite(WaterPumpPin, true);

    // deal with inverter/motor temp
    float invMotorTemp = 0.0f;
    invMotorTemp = fmaxf(invMotorTemp, inverterSensors.temperatureModuleA);
    invMotorTemp = fmaxf(invMotorTemp, inverterSensors.temperatureModuleB);
    invMotorTemp = fmaxf(invMotorTemp, inverterSensors.temperatureModuleC);
    invMotorTemp = fmaxf(invMotorTemp, inverterSensors.temperatureGateDriverBoard);
    invMotorTemp = fmaxf(invMotorTemp, inverterSensors.temperatureBoard);
    // invMotorTemp = fmaxf(invMotorTemp, inverterSensors.temperatureMotor);

    if (invMotorTemp >= COOLING_INVETERMOTOR_THRESHOLD)
    {
        // exceeds temperature, turn on the radiator
        gpioWrite(RadiatorPin, true);
    }
    else
    {
        gpioWrite(RadiatorPin, false);
    }

    // deal with accumulator
    if (bmsSensors.temperatureHigh >= COOLING_BATTERY_THRESHOLD)
    {
        // a cell exceeds the threshold
        gpioWrite(ACC1FanPin, true);
        gpioWrite(ACC2FanPin, true);
    }
    else
    {
        gpioWrite(ACC1FanPin, false);
        gpioWrite(ACC2FanPin, false);
    }

    return STATE_RUN;
}
