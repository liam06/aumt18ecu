#ifndef _COOLINGMODULE_H
#define _COOLINGMODULE_H

#include "lib/states.h"

extern state_t coolingModuleInit();
extern state_t coolingModuleUpdate();

#endif