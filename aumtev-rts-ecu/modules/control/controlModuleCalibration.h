#ifndef _CONTROLMODULECALIBRATION_H
#define _CONTROLMODULECALIBRATION_H

#include <stdbool.h>


/* Torque controller */

// How far APPS needs to be pressed before torque is generated
// Used to prevent noise from creating a control signal
static const float controlAPPSDeadZone = 0.1f;

// Max torque reachable
static const float controlTMax = 240.0f;

// Max torque reachable in eco mode
static const float controlTMaxEco = 10.0f; 


#endif