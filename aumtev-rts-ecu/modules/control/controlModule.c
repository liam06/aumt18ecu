#include "controlModule.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include "driverh/appsA/appsADriver.h"
#include "driverh/appsB/appsBDriver.h"
#include "driverh/bpsA/bpsADriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "driverl/gpio/gpio.h"
#include "driverl/can/can.h"

#include "lib/states.h"
#include "lib/queue.h"

#include "controlModuleCalibration.h"


bool appsBpsPlausability; 
uint16_t appsBpsPlausabilityCtr;

// Pin definition
const pin_t* pinBrakeLight = &P8_11;


state_t controlModuleInit()
{
	printf("LOG: modules/control: Initializing control module\n");

	controlTorque = 0.0f;
	controlTorqueIsRunning = false;  // start disabled
	appsBpsPlausability = false;
	appsBpsPlausabilityCtr = 0;

	return STATE_RUN;
}

state_t controlModuleTick()
{
	// Brake lamp
	bool brakeLampOn = bpsASensorValue > 0.5f;   // TODO: update threshold
	gpioWrite(pinBrakeLight, brakeLampOn);


	// Torque control
	if (controlTorqueIsRunning == true)
	{
		if (inverterSensors.stateInverterEnable == 0)
		{
			// inverter is still disabled
			controlModuleStart();  // send start sequence again
		}
		else
		{
			/* Get sensor data */
			float apps = 0.5f * (appsASensorValue + appsBSensorValue);
			if (apps > 1.0f)
				apps = 1.0f;
			else if (apps < 0.0f)
				apps = 0.0f;

			// dead-zone
			if (apps < 0.1f)
			{
				apps = 0.0f;
			}
			

			/* Torque model */
			controlTorque = controlTMax * apps;


			// Handle the torque degredation
			float degredation = 0.9f;
			float iOut = M_PI * controlTorque * inverterSensors.motorSpeed / (30 * bmsSensors.packInstVoltage);
			if (iOut > degredation*bmsSensors.packDCLMax)
			{
				// need to degrade the torque
				float tDegraded = 30 * bmsSensors.packInstVoltage * bmsSensors.packDCLMax / (M_PI * inverterSensors.motorSpeed);
				controlTorque = degredation * tDegraded;
			}


			// Torque encoder / brake plautibility
			if (appsBpsPlausability == true)
			{
				controlTorque = 0.0f;

				if (bpsASensorValue < 0.8f && apps < 0.1f)
				{
					// turn it back on
					appsBpsPlausability = false;
					appsBpsPlausabilityCtr = 0;
				}
			}
			else 
			{
				if (apps > 0.25f && bpsASensorValue > 0.8f) // activate the implausibility?
				{
					appsBpsPlausabilityCtr++;
					if (appsBpsPlausabilityCtr >= 20)
					{
						appsBpsPlausability = true;
					}
				}
				else
				{
					appsBpsPlausabilityCtr = 0;
				}
			}

			inverterDriverTorqueRequest(controlTorque, inverterDirectionForward);
		}
	}

	return STATE_RUN;
}

void controlModuleStart()
{
	// turn on inverter (if not already)
	if (inverterSensors.stateInverterEnable == 0)
	{
		// needs a false then true toggle
		inverterDriverSetEnabled(false);
		inverterDriverSetEnabled(true);
	}

	// start control calculation
	controlTorqueIsRunning = true;
}

void controlModuleStop()
{
	// turn off inverter
	inverterDriverSetEnabled(false);

	// stop control calculation
	controlTorqueIsRunning = false;
}