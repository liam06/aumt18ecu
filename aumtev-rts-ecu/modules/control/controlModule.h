#ifndef _CONTROLMODULE_H
#define _CONTROLMODULE_H

#include <stdint.h>
#include <stdbool.h>
#include "lib/states.h"

float controlTorque;
bool controlTorqueIsRunning;

#define STATE_CONTROL_DISABLED (state_t)0x11
#define STATE_CONTROL_RUNNING (state_t)0x12
#define STATE_CONTROL_RUNNING_INVDIS (state_t)0x13  /* Control running but inverter disabled */

extern state_t controlModuleInit();
extern state_t controlModuleTick();

/*
 * Starts torque calculation and outputs inverter control messages
 * Signals to the inverter to enable
 */
extern void controlModuleStart();

/*
 * Stops torque calculation and stops inverter control messages.
 * Signals to the inverter to disable
 */
extern void controlModuleStop();

#endif
