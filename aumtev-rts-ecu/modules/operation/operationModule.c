#include "operationModule.h"
#include "helpers.h"

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <math.h>

#include "lib/states.h"
#include "driverl/can/can.h"
#include "driverh/appsA/appsADriver.h"
#include "driverh/appsB/appsBDriver.h"
#include "driverh/bpps/bppsDriver.h"
#include "driverh/bpsA/bpsADriver.h"
#include "driverh/wheelSpeed/wheelSpeedFrontDriver.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverh/dashboard/dashboardDriver.h"
#include "driverh/canDriverInterface/canDriverInterfaceCommon.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "driverh/rtds/rtdsDriver.h"
#include "driverh/powerstageShutdown/powerstageShutdownDriver.h"
#include "modules/control/controlModule.h"


// For counting how long errors/states persist for
uint16_t stateTimingCounter1;
uint16_t stateTimingCounter2;
vehicleDriveState_t stateStore; // stored for returning from ERROR

// Transition ready?
bool isTransition_Init_ReadyToCharge();		// INIT->READY_TO_CHARGE
bool isTransition_ReadyToCharge_Charging();	// READY_TO_CHARGE->CHARGING
bool isTransition_ReadyToCharge_ReadyToRun(); // READY_TO_CHARGE->READY_TO_RUN
bool isTransition_Charging_ReadyToRun();	// CHARGING->READY_TO_DRIVE
bool isTransition_ReadyToRun_Running();		// READY_TO_DRIVE->RUNNNG
bool isTransition_Any_Error();			// *->ERROR
bool isTransition_Any_ErrorFatal();		// *->ERROR_FATAL
bool isTransition_Error_Return();			// ERROR->previous

// Checks whether a transition condition is met and stores a code
bool doCheck(bool check, uint16_t* codeOutput, uint16_t code);

// Performs the basics of a state transitions
void doTransition(const vehicleDriveState_t newState, 
				  const char* prevStateName,
				  const char* nextStateName);

// Perform transitions
void doTransition_Init_ReadyToCharge();		// INIT->READY_TO_CHARGE
void doTransition_ReadyToCharge_Charging();	// READY_TO_CHARGE->CHARGING
void doTransition_ReadyToCharge_ReadyToRun(); // READY_TO_CHARGE->READY_TO_RUN
void doTransition_Charging_ReadyToRun();	// CHARGING->READY_TO_DRIVE
void doTransition_ReadyToRun_Running();		// READY_TO_DRIVE->RUNNNG
void doTransition_Any_Error();			// *->ERROR
void doTransition_Any_ErrorFatal();		// *->ERROR_FATAL
void doTransition_Error_Return();			// ERROR->previous


state_t operationModuleInit()
{
	printf("LOG: modules/operation: Initializing system operation module\n");

	stateTimingCounter1 = 0;
	stateTimingCounter2 = 0;
	vehicleDriveState = VEHICLE_DRIVE_STATE_INIT;

	return STATE_RUN;
}

state_t operationModuleTick()
{
	switch (vehicleDriveState)
	{
		case VEHICLE_DRIVE_STATE_INIT:
		{		
			// if (isTransition_Any_ErrorFatal()) doTransition_Any_ErrorFatal();
			// else if (isTransition_Any_Error()) doTransition_Any_Error();
			if (isTransition_Init_ReadyToCharge()) doTransition_Init_ReadyToCharge();
			break;
		}
		case VEHICLE_DRIVE_STATE_READY_TO_CHARGE:
		{
			if (isTransition_Any_ErrorFatal()) doTransition_Any_ErrorFatal();
			else if (isTransition_Any_Error()) doTransition_Any_Error();
			else if (isTransition_ReadyToCharge_ReadyToRun()) doTransition_ReadyToCharge_ReadyToRun();
			else if (isTransition_ReadyToCharge_Charging()) doTransition_ReadyToCharge_Charging();
			break;
		}
		case VEHICLE_DRIVE_STATE_CHARGING:
		{
			if (isTransition_Any_ErrorFatal()) doTransition_Any_ErrorFatal();
			else if (isTransition_Any_Error()) doTransition_Any_Error();
			else if (isTransition_Charging_ReadyToRun()) doTransition_Charging_ReadyToRun();
			break;
		}
		case VEHICLE_DRIVE_STATE_READY_TO_RUN:
		{
			if (isTransition_Any_ErrorFatal()) doTransition_Any_ErrorFatal();
			else if (isTransition_Any_Error()) doTransition_Any_Error();
			else if (isTransition_ReadyToRun_Running()) doTransition_ReadyToRun_Running();
			break;
		}
		case VEHILCE_DRIVE_STATE_RUNNING:
		{
			if (isTransition_Any_ErrorFatal()) doTransition_Any_ErrorFatal();
			else if (isTransition_Any_Error()) doTransition_Any_Error();
			break;
		}
		case VEHICLE_DRIVE_STATE_ERROR:
		{
			if (isTransition_Any_ErrorFatal()) doTransition_Any_ErrorFatal();
			else if (isTransition_Any_Error()) doTransition_Any_Error();
			else if (isTransition_Error_Return()) doTransition_Error_Return();
			break;
		}
		case VEHICLE_DRIVE_STATE_ERROR_FATAL:
		{
			// No transitions from here
			break;
		}
	}


	return STATE_RUN;
}


/* *********** State machine helper functions *********** */
bool doCheck(bool check, uint16_t* codeOutput, uint16_t code)
{
	if (check)
	{
		*codeOutput |= code;
	}

	return check;
}


// Transition conditions ----------------------------------
bool isTransition_Init_ReadyToCharge()
{
	bool transition = true;
	uint16_t code = 0;

	transition &= doCheck(timerLt(&lastReceivedCanId[CAN_ID_INVERTER_VOLTAGE], 100.0e-3), &code, 0x0001); // Inverter loaded
	transition &= doCheck(timerLt(&lastReceivedCanId[CAN_ID_BMS_PACKVC], 100.0e-3), &code, 0x0002); // BMS loaded
	transition &= doCheck(timerLt(&lastReceivedCanId[CAN_ID_ECU_SENSORS1], 100.0e-3), &code, 0x0004); // Front sensor loaded 

	if (transition) printf("LOG: modules/operation: INIT->READY_TO_CHARGE tranisition - code: %x\n", code);

	return transition;
}

bool isTransition_ReadyToCharge_Charging()
{
	bool transition = true;
	uint16_t code = 0;

	// Driver is ready, and inverter needs to pre-charge?
	transition &= doCheck(dashboardStatus.driverConfirmButton == true, &code, 0x0001); // Button pressed
	// transition &= doCheck(inverterSensors.faultPOSTHi == 0x0020, &code, 0x0004);
	// transition &= doCheck(inverterSensors.faultPOSTLo == 0x0000, &code, 0x0008);
	// transition &= doCheck(inverterSensors.stateVSM == 7, &code, 0x0010);

	if (transition) printf("LOG: modules/operation: READY_TO_CHARGE->CHARGING tranisition - code: %x\n", code);

	return transition;
}

bool isTransition_ReadyToCharge_ReadyToRun()
{
	bool transition = true;
	uint16_t code = 0;

	// Driver is ready, and inverter DOES NOT need to pre-charge?
	transition &= doCheck(dashboardStatus.driverConfirmButton == true, &code, 0x0001); // Button pressed
	// transition &= doCheck(bpsASensorValue > 0.5f, &code, 0x0002);  // brake is pressed (at least 50%)
	transition &= doCheck(inverterSensors.faultPOSTHi == 0x0000, &code, 0x0004);
	transition &= doCheck(inverterSensors.faultPOSTLo == 0x0000, &code, 0x0008);
	transition &= doCheck(inverterSensors.stateVSM == 4, &code, 0x0010);

	if (transition) printf("LOG: modules/operation: READY_TO_CHARGE->READY_TO_RUN tranisition - code: %x\n", code);

	return transition;
}

bool isTransition_Charging_ReadyToRun()
{
	bool transition = true;
	uint16_t code = 0;

	transition &= doCheck(inverterSensors.stateVSM == 4, &code, 0x0001);

	if (transition) printf("LOG: modules/operation: CHARGING->READY_TO_RUN tranisition - code: %x\n", code);
	return transition;
}

bool isTransition_ReadyToRun_Running()
{
	bool transition = true;
	uint16_t code = 0;

	transition &= doCheck(dashboardStatus.driverConfirmButton == true, &code, 0x0001); // Button pressed
	transition &= doCheck(bpsASensorValue > 0.8f, &code, 0x0002);  // brake is pressed (at least 50%)

	// float appsAvg = 0.5f * (appsASensorValue + appsBSensorValue);
	// transition &= doCheck(appsASensorValue < 0.1f, &code, 0x0004);  // APPS not pressed (less than 5%)

	if (transition) printf("LOG: modules/operation: READY_TO_RUN->RUNNING tranisition - code: %x\n", code);

	return transition;
}

bool isTransition_Any_Error()
{
	bool transition = false;
	bool sensorErrorInst = false;   // is there an error on the current sample?
	uint16_t code = 0;

	// Sensor errors
	sensorErrorInst |= doCheck(valuesDiffBy(appsASensorValue, appsBSensorValue, 0.1f), &code, 0x0001);
	// sensorErrorInst |= doCheck(appsASensorValue > 0.25f && bpsASensorValue > 0.5f, &code, 0x0004);
	sensorErrorInst |= doCheck(appsASensorValue > 1.8f, &code, 0x0008);
	sensorErrorInst |= doCheck(appsASensorValue < -0.5f, &code, 0x0010);
	sensorErrorInst |= doCheck(appsBSensorValue > 1.8f, &code, 0x0020);
	sensorErrorInst |= doCheck(appsBSensorValue < -0.5f, &code, 0x0040);
	sensorErrorInst |= doCheck(bpsASensorValue > 1.8f, &code, 0x0080);
	sensorErrorInst |= doCheck(bpsASensorValue < -0.4f, &code, 0x0100);

	if (sensorErrorInst)
		++stateTimingCounter1;
	else
		stateTimingCounter1 = 0;
	
	transition |= doCheck(stateTimingCounter1 >= 50, &code, 0x0200);  // persisting for 100ms

	// Device connection errors
	transition |= doCheck(!timerLt(&lastReceivedCanId[CAN_ID_INVERTER_VOLTAGE], 200.0e-3), &code, 0x0400); // still receiving inverter
	transition |= doCheck(!timerLt(&lastReceivedCanId[CAN_ID_BMS_PACKVC], 200.0e-3), &code, 0x0800); // still receiving BMS
	transition |= doCheck(!timerLt(&lastReceivedCanId[CAN_ID_ECU_SENSORS1], 600.0e-3), &code, 0x1000); // still receving front ECU

	// Error inputs
	transition |= doCheck(sysWarningStatus.errorBMS, &code, 0x2000);
	transition |= doCheck(sysWarningStatus.errorBSPD, &code, 0x4000);
	transition |= doCheck(sysWarningStatus.errorPDOC, &code, 0x8000);

	if (transition) printf("LOG: modules/operation: ->ERROR tranisition - code: %x\n", code);

	return transition;
}

bool isTransition_Any_ErrorFatal()
{
	bool transition = false;
	bool valueErrorInst = false;
	uint16_t code = 0;

	// these must persist for a few samples to trip an error
	// valueErrorInst |= doCheck(bmsSensors.packInstPower > 80.0e3f, &code, 0x0001);
	// valueErrorInst |= doCheck(bmsSensors.temperatureHigh > 60.0f, &code, 0x0002); 

	valueErrorInst |= doCheck(fabsf(bmsSensors.packInstPower) > 5e3 && bpsASensorValue > 1.2f, &code, 0x0004);

	if (valueErrorInst)
		++stateTimingCounter2;
	else
		stateTimingCounter2 = 0;
	
	transition |= doCheck(stateTimingCounter2 >= 50, &code, 0x0008); // persisting for 500ms
	// transition |= doCheck(bmsSensors.packSOC <= 10, &code, 0x0010); // less than 10% SOC

	// Fatal error inputs
	transition |= doCheck(sysWarningStatus.errorIMD, &code, 0x0020);

	if (transition) printf("LOG: modules/operation: ->ERROR_FATAL tranisition - code: %x\n", code);

	if (transition && valueErrorInst)  // turn on BSPD light
	{
		sysWarningStatus.errorBSPD = true;
	}

	return transition;
}

bool isTransition_Error_Return()
{
	bool transition = ++stateTimingCounter1 == 200;  // 2 seconds
	return transition;
}



// Transition actions -------------------------------------
void doTransition(const vehicleDriveState_t newState, 
				  const char* prevStateName,
				  const char* nextStateName)
{
	stateStore = vehicleDriveState;  // copy the state for transitions out of ERROR
	stateTimingCounter1 = 0;   // reset timer
	stateTimingCounter2 = 0;

	vehicleDriveState = newState;
	printf("LOG: modules/operation: State transition %s->%s\n", prevStateName, nextStateName);

}

void doTransition_Init_ReadyToCharge()
{
	// nothing
	doTransition(VEHICLE_DRIVE_STATE_READY_TO_CHARGE, "INIT", "READY_TO_CHARGE");
}

void doTransition_ReadyToCharge_Charging()
{
	sysWarningStatus.errorECU = 0; // Set errorECU = 0
	inverterDriverClearFaults();  // clear the faults (i.e. do precharge)

	doTransition(VEHICLE_DRIVE_STATE_CHARGING, "READY_TO_CHARGE", "CHARGING");
}

void doTransition_ReadyToCharge_ReadyToRun()
{
	sysWarningStatus.errorECU = 0;
	doTransition(VEHICLE_DRIVE_STATE_READY_TO_RUN, "READY_TO_CHARGE", "READY_TO_RUN");
}

void doTransition_Charging_ReadyToRun()
{
	// nothing
	doTransition(VEHICLE_DRIVE_STATE_READY_TO_RUN, "CHARGING", "READY_TO_RUN");
}

void doTransition_ReadyToRun_Running()
{
	// Run transition code
	sysWarningStatus.errorECU = 0; // Set errorECU = 0
	rtdsDriverPlay(); // sound RTDS
	controlModuleStart();  // turn torque output on

	doTransition(VEHILCE_DRIVE_STATE_RUNNING, "READY_RUN", "RUNNING");
}

void doTransition_Any_Error()
{
	sysWarningStatus.errorECU = 1; // Set errorECU = 1
	controlModuleStop();  // turn torque output off
	// powerstageShutdownDriverSetEnabled(false); // open shutdown circuit

	doTransition(VEHICLE_DRIVE_STATE_ERROR, "?", "ERROR");
}

void doTransition_Any_ErrorFatal()
{
	sysWarningStatus.errorECU = 1; // Set errorECU = 1
	controlModuleStop();  // turn torque output off
	powerstageShutdownDriverSetEnabled(false); // open shutdown circuit

	doTransition(VEHICLE_DRIVE_STATE_ERROR_FATAL, "?", "ERROR_FATAL");
}

void doTransition_Error_Return()
{
	sysWarningStatus.errorECU = 0;

	// doTransition(VEHICLE_DRIVE_STATE_INIT, "ERROR", "stored");
}

