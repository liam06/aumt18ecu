#include "helpers.h"

#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

bool timerLt(struct timespec* t, double dt)
{
    struct timespec timeNow;
    double diff;
    clock_gettime(CLOCK_MONOTONIC, &timeNow); // get time now
    diff = ((double)timeNow.tv_sec + 1.0e-9*timeNow.tv_nsec) - 
         ((double)t->tv_sec + 1.0e-9*t->tv_nsec);
    
    return diff <= dt;
}

bool valuesDiffBy(float a, float b, float diffPercent)
{
    // float absDiff = fabsf(a - b);
    // float divider = (a + b) / 2.0f;

    // float pDiff = absDiff / divider;
    float pDiff = fabsf(a-b);
    return pDiff > diffPercent;
}

void printfIf(bool print, const char* text)
{
    if (print)
    {
        printf(text);
    }
}