#ifndef _OPERATIONHELPERS_H
#define _OPERATIONHELPERS_H

#include <stdbool.h>
#include <time.h>

/*
 * Returns true if time difference between now and t is
 * less than or equal to dt.
 */
extern bool timerLt(struct timespec* t, double dt);


/*
 * Returns true if a and b differ by more than a given
 * percentage. Input percentages as decimal value,
 * e.g. 10% = 0.1
 * Returns false if the values are within the percentage. 
 */
extern bool valuesDiffBy(float a, float b, float diffPercent);

/*
 * If conditions satisfied, print
 */
extern void printfIf(bool print, const char* text);

#endif