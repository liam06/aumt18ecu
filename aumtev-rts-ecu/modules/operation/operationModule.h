#ifndef _OPERATIONMODULE_H
#define _OPERATIONMODULE_H

#include <stdint.h>

#include "lib/states.h"

// Vehicle drive state
typedef uint8_t vehicleDriveState_t;
#define VEHICLE_DRIVE_STATE_INIT            (vehicleDriveState_t)0
#define VEHICLE_DRIVE_STATE_READY_TO_CHARGE (vehicleDriveState_t)1
#define VEHICLE_DRIVE_STATE_CHARGING        (vehicleDriveState_t)2
#define VEHICLE_DRIVE_STATE_READY_TO_RUN    (vehicleDriveState_t)3
#define VEHILCE_DRIVE_STATE_RUNNING         (vehicleDriveState_t)4
#define VEHICLE_DRIVE_STATE_ERROR           (vehicleDriveState_t)5
#define VEHICLE_DRIVE_STATE_ERROR_FATAL     (vehicleDriveState_t)6
vehicleDriveState_t vehicleDriveState;



extern state_t operationModuleInit();
extern state_t operationModuleTick();

#endif
