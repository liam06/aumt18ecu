#include "system/sysInit.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#include "driverh/canDriverInterface/canDriverInterface.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverh/wheelSpeed/wheelSpeedFrontDriver.h"
#include "driverh/wheelSpeed/wheelSpeedRearDriver.h"
#include "driverh/appsA/appsADriver.h"
#include "driverh/appsB/appsBDriver.h"
#include "driverh/bpps/bppsDriver.h"
#include "driverh/bpsA/bpsADriver.h"
#include "driverh/bpsB/bpsBDriver.h"
#include "driverh/powerstageShutdown/powerstageShutdownDriver.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "driverh/dashboard/dashboardDriver.h"
#include "driverh/rtds/rtdsDriver.h"

#include "modules/control/controlModule.h"
#include "modules/operation/operationModule.h"
#include "modules/cooling/coolingModule.h"
#include "modules/datalog/datalogModule.h"


typedef struct {
    uint16_t appsASensorValueRaw;
    uint16_t appsBSensorValueRaw;
    uint16_t bpsASensorValueRaw;
    bool errorIMD;
    bool errorBMS;
    bool errorBSPD;
    bool errorPDOC;
    bool errorECU;
    bool driverConfirmButton;
    vehicleDriveState_t vehicleDriveState;
} saveDataEcuSensors_t;
typedef struct {
    struct timespec t;
    saveDataEcuSensors_t ecuSensors;
    inverterSensorsRaw_t inverterSensors;
    bmsSensorsRaw_t bmsSensors;
} saveData_t;



int main(int argc, char *argv[])
{
	size_t elementSize = sizeof(saveData_t);
	
	// Open the file
	FILE *f = fopen("/home/debian/logs/sensors.dat", "rb");
	if (f == NULL)
	{
		printf("couldn't open file\n");
		return -1;
	}

	// calculate the number of rows
	fseek(f, 0L, SEEK_END);
	size_t sz = ftell(f);
	fseek(f, 0L, SEEK_SET);
	
	int rows = sz / sizeof(saveData_t);
	printf("size: %u\n", sz);
	printf("N rows: %d\n", rows);
	
	// read data from file into 2D array structure
	saveData_t* array;
	array = malloc(sizeof(*array)*rows);
	fread(array, elementSize*rows, 1, f);
	
	// Print
	int i;
	for (i = 0; i < rows; i++) {
		printf("%u\n", array[i].ecuSensors.bpsASensorValueRaw);
	}

	return 0;   // Shouldn't have reached here
}
