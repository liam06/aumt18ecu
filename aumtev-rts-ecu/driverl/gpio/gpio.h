#ifndef _GPIO_H
#define _GPIO_H

#include <stdint.h>
#include <stdbool.h>
#include "lib/states.h"

typedef enum {
    gpio_dir_in,
    gpio_dir_out
} gpio_direction_t;

typedef struct {
    char *name;   /*!< readable name of pin, i.e.: "GPIO1_21", see beaglebone user guide */
    unsigned int gpio_bank; /*!< which of the four gpio banks is this pin in, i.e.: GPIO1, r 0x4804C000 */
    uint8_t gpio; /*!< pin number on the am335x processor */
    uint8_t bank_id; /*!< pin number within each bank, should be 0-31 */
    char *mux;    /*!< file name for setting mux */
    uint8_t eeprom; /*!< position in eeprom */
} pin_t;


/* *** Declare GPIO Pins *** */
extern const pin_t USR0;
extern const pin_t USR1;
extern const pin_t USR2;
extern const pin_t USR3;
extern const pin_t P8_3;
extern const pin_t P8_4;
extern const pin_t P8_5;
extern const pin_t P8_6;
extern const pin_t P8_7;
extern const pin_t P8_8;
extern const pin_t P8_9;
extern const pin_t P8_10;
extern const pin_t P8_11;
extern const pin_t P8_12;
extern const pin_t P8_13;
extern const pin_t P8_14;
extern const pin_t P8_15;
extern const pin_t P8_16;
extern const pin_t P8_17;
extern const pin_t P8_18;
extern const pin_t P8_19;
extern const pin_t P8_20;
extern const pin_t P8_21;
extern const pin_t P8_22;
extern const pin_t P8_23;
extern const pin_t P8_24;
extern const pin_t P8_25;
extern const pin_t P8_26;
extern const pin_t P8_27;
extern const pin_t P8_28;
extern const pin_t P8_29;
extern const pin_t P8_30;
extern const pin_t P8_31;
extern const pin_t P8_32;
extern const pin_t P8_33;
extern const pin_t P8_34;
extern const pin_t P8_35;
extern const pin_t P8_36;
extern const pin_t P8_37;
extern const pin_t P8_38;
extern const pin_t P8_39;
extern const pin_t P8_40;
extern const pin_t P8_41;
extern const pin_t P8_42;
extern const pin_t P8_43;
extern const pin_t P8_44;
extern const pin_t P8_45;
extern const pin_t P8_46;

extern const pin_t P9_11;
extern const pin_t P9_12;
extern const pin_t P9_13;
extern const pin_t P9_14;
extern const pin_t P9_15;
extern const pin_t P9_16;
extern const pin_t P9_17;
extern const pin_t P9_18;
extern const pin_t P9_19;
extern const pin_t P9_20;
extern const pin_t P9_21;
extern const pin_t P9_22;
extern const pin_t P9_23;
extern const pin_t P9_24;
extern const pin_t P9_25;
extern const pin_t P9_26;
extern const pin_t P9_27;
extern const pin_t P9_28;
extern const pin_t P9_29;
extern const pin_t P9_30;
extern const pin_t P9_31;
extern const pin_t P9_33;
extern const pin_t P9_35;
extern const pin_t P9_36;
extern const pin_t P9_37;
extern const pin_t P9_38;
extern const pin_t P9_39;
extern const pin_t P9_40;
extern const pin_t P9_41;
extern const pin_t P9_42;
/* ************************ */

extern state_t gpioInit();
extern state_t gpioInitPin(const pin_t* pin, gpio_direction_t direction); // Init a SINGLE GPIO pin
extern int gpioWrite(const pin_t* pin, bool value);

/*
 * Parameters:
 *  pin GPIO pin number
 *  *value  Pointer to boolean to store pin state
 * Returns:
 *  -1  Error
 *  0   Success
 */
extern int gpioRead(const pin_t* pin, bool *value);

#endif