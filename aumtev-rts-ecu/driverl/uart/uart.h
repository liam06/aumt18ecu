#ifndef _UART_H
#define _UART_H

#include <stdlib.h>
#include <stdint.h>
#include "lib/states.h"

#define UART_NUM_PORTS 5

extern state_t uartInit();  // init all used UART ports
extern state_t uartInitPort(const uint8_t portNum);  // open specific port number

// Returns < 0 if error, length of data otherwise
extern int uartWrite(const uint8_t portNum, void* data, size_t dataLen);

// Returns 0 if nothing to read, otherwise size of data, < 0 if error
// uartRead not implemented yet
// extern int uartRead(const uint8_t portNum, void* dataOut);

#endif