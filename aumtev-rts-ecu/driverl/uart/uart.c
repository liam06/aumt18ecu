#include "uart.h"

#include <stdio.h>
#include <stdint.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>

#include "lib/states.h"
#include "lib/queue.h"
#include "system/tasks.h"

int fileHandles[UART_NUM_PORTS] = {0};

// #define UART_BUFFER_SIZE 150
// // have a buffer for each port
// void* uartReceiveBuffer[UART_NUM_PORTS][UART_BUFFER_SIZE];
// queueInfo_t uartReceiveBufferInfo[UART_NUM_PORTS];

// void *uartTask(void *vargp);

state_t uartInit() 
{
    // Just init the in use ports
    uint8_t ports[] = {4};

    uint8_t i;
    for (i = 0; i < sizeof(ports)/sizeof(ports[0]); i++)
    {
        printf("LOG: driverl/uart: Initializing UART%u", ports[i]);
        if (uartInitPort(ports[i]) == STATE_ERROR)
        {
            printf("ERROR: driverl/uart: Could not init UART%u\n", ports[i]);
            return STATE_ERROR;
        }
    }

    return STATE_RUN;
}

state_t uartInitPort(const uint8_t portNum)
{
    if (portNum >= UART_NUM_PORTS)
    {
        printf("ERROR: driverl/uart: Port number %u too high\n", portNum);
        return STATE_RUN;
    }

    char path[15];
    sprintf(path, "/dev/tty0%u", portNum);

    if ((fileHandles[portNum] = open(path, O_RDWR | O_NOCTTY | O_NDELAY))<0)
    {
        perror("ERROR: driverl/uart: Failed to open the file.\n");
        return STATE_ERROR;
    }

    struct termios options;
    tcgetattr(fileHandles[portNum], &options);
    options.c_cflag = B57600 | CS8 | CREAD | CLOCAL;
    options.c_iflag = IGNPAR | ICRNL;
    options.c_ispeed = 9600; // TODO update
    options.c_ospeed = 9600; 
    tcflush(fileHandles[portNum], TCIFLUSH);
    tcsetattr(fileHandles[portNum], TCSANOW, &options);

    // create buffer
	// if (queueInit(&uartReceiveBufferInfo, (void*)uartWriteBuffer, CAN_BUFFER_SIZE, sizeof(struct can_frame)) == false)
	// {
	// 	printf("ERROR: driverl/can: Failed to init CAN write buffer\n");
	// }

    // start monitor thread
    // pthread_t threadTask_id;
    // uint8_t portNumVal = portNum;
    // void *portNumPtr = &portNumVal;
	// if (taskCreateRR(&threadTask_id, uartTask, (void*)portNumPtr) == -1)
	// {
	// 	printf("ERROR: driverl/uart: Could not set up background thread\n");
	// }

    return STATE_RUN;
}

int uartWrite(const uint8_t portNum, void* data, size_t dataLen)
{
    if (portNum >= UART_NUM_PORTS)
    {
        // port num is too high
        return -2;
    }
    if (fileHandles[portNum] == 0)
    {
        // not initialized
        return -3;
    }

    return write(fileHandles[portNum], data, dataLen);
}

// int uartRead(const uint8_t portNum, void* dataOut)
// {
//     return -1;
// }
