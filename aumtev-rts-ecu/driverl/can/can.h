#ifndef _CAN_H
#define _CAN_H

#include <stdint.h>
#include "lib/states.h"

extern state_t canState;

extern state_t canInit();
extern state_t canUpdate();

extern void canWrite(const uint16_t can_id, const uint8_t len, const uint8_t *data); 

/*
 * canRead: Reads messages stored in internal buffer from CAN socket.
 * Will return oldest element in buffer.
 * Parameters:
 *      uint8_t *data   Array of data (max length of 8) (output)
 *      uint16_t can_id CAN ID (output)
 * Returns:
 *      Length of data array (max of 8).
 */
extern uint8_t canRead(uint8_t *data, uint16_t *can_id); // TODO: update function prototypes

#endif