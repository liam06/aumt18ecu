#include "can.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "linux/can.h"
#include "linux/can/raw.h"

#include "lib/states.h"
#include "lib/queue.h"
#include "system/tasks.h"

state_t canState = STATE_INIT;

// Variables used by SocketCAN
int s;

// Recieve Buffer
#define CAN_BUFFER_SIZE 15000
struct can_frame canReceiveBuffer[CAN_BUFFER_SIZE];
queueInfo_t canReceiveBufferInfo;
struct can_frame canWriteBuffer[CAN_BUFFER_SIZE];
queueInfo_t canWriteBufferInfo;

// for managing error outputs
struct timespec timeStart = {0,0};


uint32_t canResetTimer = 0;
bool canResetThread = false;


void *canTask(void *vargp);


state_t canInit()
{
	printf("LOG: driverl/can: Initializing CAN\n");

	// Set up CAN driver
	struct sockaddr_can addr;
	struct ifreq ifr;

	const char *ifname = "can0";

	if((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("ERROR: driverl/can: Error while opening socket");
		canState = STATE_ERROR;
		return -1;
	}

	strcpy(ifr.ifr_name, ifname);
	ioctl(s, SIOCGIFINDEX, &ifr);
	
	addr.can_family  = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	printf("LOG: driverl/can: %s at index %d\n", ifname, ifr.ifr_ifindex);

	if(bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("ERROR: driverl/can: Error in socket bind");
		canState = STATE_ERROR;
		return STATE_ERROR;
	}
	
	// Set up CAN socket threads
	if (queueInit(&canReceiveBufferInfo, (void*)canReceiveBuffer, CAN_BUFFER_SIZE, sizeof(struct can_frame)) == false)
	{
		printf("ERROR: driverl/can: Failed to init CAN receive buffer\n");
		canState = STATE_ERROR;
		return STATE_ERROR;
	}
	if (queueInit(&canWriteBufferInfo, (void*)canWriteBuffer, CAN_BUFFER_SIZE, sizeof(struct can_frame)) == false)
	{
		printf("ERROR: driverl/can: Failed to init CAN write buffer\n");
		canState = STATE_ERROR;
		return STATE_ERROR;
	}

	pthread_t threadTask_id;
	if (taskCreateRR(&threadTask_id, canTask, NULL, 20) == -1)
	{
		printf("ERROR: driverl/can: Could not set up background thread\n");
		canState = STATE_ERROR;
		return STATE_ERROR;
	}

	canState = STATE_RUN;
	return STATE_RUN;
}

state_t canUpdate()
{
	// prevent CAN delay bug
	if (++canResetTimer == 1000)   // 30 seconds
	{
		// reset CAN
		canResetThread = true;  // terminate secondary thread
		canInit();

		canResetTimer = 0;
	}

	return STATE_RUN;
}

void canWrite(const uint16_t can_id, const uint8_t len, const uint8_t *data)
{
	if (canState == STATE_RUN)
	{
		struct can_frame frame;
		frame.can_id = can_id;
		frame.can_dlc = len;

		unsigned int i;
		for (i = 0; i < len; ++i)
		{
			frame.data[i] = data[i];
		}

		int retval = write(s, &frame, sizeof(frame));
		if (retval < 0)
		{
			// only print error messages every half second
			struct timespec timeNow;
			double diff;
			clock_gettime(CLOCK_MONOTONIC, &timeNow); // get time now
			diff = ((double)timeNow.tv_sec + 1.0e-9*timeNow.tv_nsec) - 
				((double)timeStart.tv_sec + 1.0e-9*timeStart.tv_nsec);

			if (diff >= 0.5)
			{
				printf("WARNING: driverl/can: Error sending message with id %x - %s\n", frame.can_id, strerror(errno));

				if (errno == ENOBUFS) { 
					perror("ERROR: driverl/can: write failed - buffer");
				} else { 
					perror("ERROR: driverl/can: write failed"); 
				}

				clock_gettime(CLOCK_MONOTONIC, &timeStart);
			}	
		}

		// put this in the buffer
		// if (canWriteBufferInfo.bufferCount == canWriteBufferInfo.bufferSize)
		// {
		// 	printf("WARNING: driverl/can: Error putting CAN message in internal buffer - buffer full\n");
		// 	return;
		// }

		// bool ret = queueInsert(&canWriteBufferInfo, (void*)&frame);
		// if (!ret)
		// {
		// 	printf("WARNING: driverl/can: Error putting CAN message in internal buffer\n");
		// }
	}
}

uint8_t canRead(uint8_t *data, uint16_t * can_id)
{
	if (queueLen(&canReceiveBufferInfo) == 0)
	{
		return 0;
	}

	struct can_frame val;
	bool res = queuePop(&canReceiveBufferInfo, (void*)&val);

	if (!res)
	{
		return 0;
	}

	// output data
	memcpy(data, val.data, val.can_dlc);
	*can_id = val.can_id;

    return val.can_dlc;
} 

void *canTask(void *vargp)
{
	printf("LOG: driverl/can: Monitor CAN task ready\n");


	if (canState == STATE_RUN) // IMPORTANT: CAN bus must be finished initializing
	{
		while(true)
		{
			if (canResetThread == true)
			{
				canResetThread = false;
				return NULL;
			}

			// struct timeval timeout = {1, 0};  // timeout for 1s
			fd_set readSet;
			FD_ZERO(&readSet);
			FD_SET(s, &readSet);

			// timeout.tv_sec = 0;
			// timeout.tv_usec = 10000; // microseconds

			// int rc = select(s + 1, &readSet, NULL, NULL, &timeout);

			// rc == 0 - timeout
			// if (!rc) {
			// 	// write your CAN frame
			// 	if (queueLen(&canWriteBufferInfo) > 0) // write if there is more left in the buffer
			// 	{
			// 		struct can_frame frame;
			// 		bool ret = queuePop(&canWriteBufferInfo, (void*)&frame);

			// 		if (!ret)
			// 		{
			// 			printf("WARNING: driverl/can: Error retrieving message from CAN write buffer\n");
			// 		}
			// 		else
			// 		{
			// 			int retval = write(s, &frame, sizeof(frame));
			// 			if (retval < 0)
			// 			{
			// 				printf("WARNING: driverl/can: Error sending message with id %x - %s\n", frame.can_id, strerror(errno));

			// 				if (errno == ENOBUFS) { 
			// 					sched_yield(); 
			// 					printf("BUFFER\n");
			// 				} else { 
			// 					perror("write failed"); 
			// 				}	
			// 			}
			// 		}
			// 	}
			// }

			if (FD_ISSET(s, &readSet)) {
				// read CAN frames
				struct can_frame receiveMsg;      
				int recvbytes = read(s, &receiveMsg, sizeof(receiveMsg));

				if (recvbytes == -1)
				{
					// only print error messages every half second
					struct timespec timeNow;
					double diff;
					clock_gettime(CLOCK_MONOTONIC, &timeNow); // get time now
					diff = ((double)timeNow.tv_sec + 1.0e-9*timeNow.tv_nsec) - 
						((double)timeStart.tv_sec + 1.0e-9*timeStart.tv_nsec);

					if (diff >= 0.5)
					{
						printf("WARNING: driverl/can: Error reading CAN message\n");

						clock_gettime(CLOCK_MONOTONIC, &timeStart);
					}
				}
				else
				{
					// finally, store in our buffer
					bool res = queueInsert(&canReceiveBufferInfo, (void*)&receiveMsg);

					if (!res)
					{
						// only print error messages every half second
						struct timespec timeNow;
						double diff;
						clock_gettime(CLOCK_MONOTONIC, &timeNow); // get time now
						diff = ((double)timeNow.tv_sec + 1.0e-9*timeNow.tv_nsec) - 
							((double)timeStart.tv_sec + 1.0e-9*timeStart.tv_nsec);

						if (diff >= 0.5)
						{
							printf("WARNING: driverl/can: Could not store CAN message in internal buffer\n");

							clock_gettime(CLOCK_MONOTONIC, &timeStart);
						}
					}
				}
			}
		}
	}

	return NULL;
}
