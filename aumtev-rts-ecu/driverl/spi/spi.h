#ifndef _SPI_H
#define _SPI_H

/*
 * This code will operate SPI mode 0 on device /dev/spidev1.x
 */

#include <stdint.h>
#include "lib/states.h"
#include "driverl/gpio/gpio.h"

extern state_t spiInit();    // Initialize SPI bus (spidev1.x)
extern state_t spiInitDev(); // Add device (CS pins, etc.) to SPI bus
extern void pabort(const char *s);
extern void spiTransfer(uint8_t *rx, uint8_t *tx, const uint8_t dataLen, const pin_t* csPin);

#endif