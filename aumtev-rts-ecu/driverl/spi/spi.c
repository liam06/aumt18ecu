#include "spi.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <stdlib.h>

#include "lib/states.h"
#include "driverl/gpio/gpio.h"

state_t spiState = STATE_INIT;

// Internal config for SPI driver
static const char *device = "/dev/spidev1.0";   // 2 for rear, 1 for front
static uint8_t mode = 0;
static uint8_t bits = 8;
static uint8_t firstBit = 0; // 0=MSB first, 1=LSB first
static uint32_t speed = 1600000;
int fd;

state_t spiInit()
{
    printf("LOG: driverl/spi: Initializing SPI\n");

    int ret = 0;

    // Open SPI device
	fd = open(device, O_RDWR);
	if (fd < 0)
		pabort("ERROR: driverl/spi: can't open device");

	// SPI mode
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't get spi mode");

	// Bits per word
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't get bits per word");

	// MSB first
	ret = ioctl(fd, SPI_IOC_WR_LSB_FIRST, &firstBit);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't set MSB first");

	ret = ioctl(fd, SPI_IOC_RD_LSB_FIRST, &firstBit);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't get MSB first");

	// Max speed (Hz)
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't get max speed hz");
    
	printf("LOG: driverl/spi: Mode: %d\n", mode);
	printf("LOG: driverl/spi: Bits per word: %d\n", bits);
	printf("LOG: driverl/spi: Max speed: %d Hz (%d KHz)\n", speed, speed/1000);

	// close(fd);

    spiState = STATE_RUN;
    return spiState;
}

state_t spiInitDev()
{
	return STATE_RUN;
}

void pabort(const char *s)
{
	perror(s);
	abort();
}

void spiTransfer(uint8_t *rx, uint8_t *tx, const uint8_t dataLen, const pin_t* csPin)
{
	int ret;

	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = dataLen,
		.delay_usecs = 0,
		.speed_hz = 0,
		.bits_per_word = 0,
	};

	gpioWrite(csPin, false);
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	gpioWrite(csPin, true);
	if (ret == -1)
		pabort("ERROR: driverl/spi: can't send spi message");
}
