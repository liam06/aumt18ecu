#include "adc.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "driverl/spi/spi.h"
#include "driverl/gpio/gpio.h"
#include "lib/states.h"
#include "lib/bitsplit.h"

state_t adcState = STATE_INIT;

#define NUM_ADCS 1
#define NUM_CHANNELS 8   /* number of channels per ADC */

// Defines which pins the device indices map to
const pin_t* ADC_PINS_DEVIDX[NUM_ADCS] = {
    &P9_15, // devIdx=0 => CS GPIO=48
    // &P9_16, // devIdx=1 => CS GPIO=51
    // &P9_14, // devIdx=2 => CS GPIO=40
};

// DEFINITION OF CONNECTED ANALOG INPUTS --------
const ain_t AIN0 = {.devIdx=0, .pinIdx=0};
const ain_t AIN1 = {.devIdx=0, .pinIdx=1};
const ain_t AIN2 = {.devIdx=0, .pinIdx=2};
const ain_t AIN3 = {.devIdx=0, .pinIdx=3};
const ain_t AIN4 = {.devIdx=0, .pinIdx=4};
const ain_t AIN5 = {.devIdx=0, .pinIdx=5};
const ain_t AIN6 = {.devIdx=0, .pinIdx=6};
const ain_t AIN7 = {.devIdx=0, .pinIdx=7};
// const ain_t AIN8 = {.devIdx=1, .pinIdx=0};
// const ain_t AIN9 = {.devIdx=1, .pinIdx=1};
// const ain_t AIN10 = {.devIdx=1, .pinIdx=2};
// const ain_t AIN11 = {.devIdx=1, .pinIdx=3};
// const ain_t AIN12 = {.devIdx=1, .pinIdx=4};
// const ain_t AIN13 = {.devIdx=1, .pinIdx=5};
// const ain_t AIN14 = {.devIdx=1, .pinIdx=6};
// const ain_t AIN15 = {.devIdx=1, .pinIdx=7};
// ----------------------------------------------

uint16_t adcBuffer[NUM_ADCS][NUM_CHANNELS];

state_t adcInit()
{
    printf("LOG: driverl/adc: Initializing ADC (MCP3208)\n");

	// Init the extra GPIO pins for chip-select
    unsigned int i;
    for (i = 0; i < NUM_ADCS; ++i)
    {
        gpioInitPin(ADC_PINS_DEVIDX[i], gpio_dir_out);	// CS0
	    gpioWrite(ADC_PINS_DEVIDX[i], true); // Mode 0 => !CS = 1 for not selected
    }

    // initialize buffer
    memset(adcBuffer, 0, sizeof adcBuffer);

    adcState = STATE_RUN;
    return adcState;
}

state_t adcUpdate()
{
    if (adcState == STATE_RUN)
    {
        int i, j;
        for (i = 0; i < NUM_ADCS; i++)   // loop through the 3 devices
        {
            for (j = 0; j < NUM_CHANNELS; j++)   // loop through the 8 pins/device
            {
                // input config:
                uint8_t cfgStartBit = 1;  // just a bit for starting
                uint8_t cfgSingle = 1;  // 1 means single-ended measurement
                uint8_t cfgChannel = 0b00000111 & j; // mask to 3 bits

                // construct SPI message
                uint8_t adcTx[3] = {0};
                uint8_t adcRx[3];

                adcTx[0] |= cfgStartBit << 2;
                adcTx[0] |= cfgSingle << 1;
                adcTx[0] |= cfgChannel >> 2; // only want top bit (D2)
                adcTx[1] |= cfgChannel << 6; // only want bottom two bits at top

                spiTransfer(adcRx, adcTx, 3, ADC_PINS_DEVIDX[i]);

                // extract data
                uint16_t data = (uint16_t)combineBytesToInt(adcRx+1, 2, endianBig);
                data &= 0x0FFF;  // mask with 12 bits

                // store data
                adcBuffer[i][j] = data;
            }
        }
    }

    return adcState;
}

uint16_t adcRead(const ain_t *adcDevice)
{
    if (adcState == STATE_RUN) {
        // get the adc data out of the buffer
        return adcBuffer[adcDevice->devIdx][adcDevice->pinIdx];
    }
    else
    {
        return UINT16_MAX;
    }
}

int8_t adcReadDigital(const ain_t *adcDevice)
{
    uint16_t adcVal = adcRead(adcDevice);

    // use thresholds to convert voltage to binary value
    if (adcVal >= ADC_DLOW_TL && adcVal <= ADC_DLOW_TH)         return 0; // low
    else if (adcVal >= ADC_DHIGH_TL && adcVal <= ADC_DHIGH_TH)  return 1;  // high
    else return -1; //error
}