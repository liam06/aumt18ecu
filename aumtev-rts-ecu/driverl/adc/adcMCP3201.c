#include "adc.h"

#include <stdio.h>
#include <stdint.h>

#include "driverl/spi/spi.h"
#include "driverl/gpio/gpio.h"
#include "lib/states.h"

state_t adcState = STATE_INIT;

uint16_t data[1][12];  // indicies: [devIdx][pinIdx]

int adcInit()
{
    printf("LOG: driverl/adc: Initializing ADC (MCP3201)\n");

	// Init the extra GPIO pins for chip-select
	gpioInitPin(48, gpio_dir_out);	// CS0
	gpioWrite(48, true); // Mode 0 => !CS = 1 for not selected
	gpioInitPin(51, gpio_dir_out);	// CS0
	gpioWrite(51, true); // Mode 0 => !CS = 1 for not selected

    adcState = STATE_RUN;
    return adcState;
}

int adcUpdate()
{
    return adcState;
}

uint16_t adcRead(const ain_t *adcDevice)
{
    if (adcState == STATE_RUN) {
        // Perform SPI comms to ADC
        uint8_t tx[] = {0x00, 0x00};  // Need two bytes of data to send
        uint8_t rx[2];
        spiTransfer(rx, tx, 2, 48);

        // Bit converstion
        uint16_t result;
        result = rx[0];
        result = result << 8;
        result = result | rx[1];
        result = result >> 1;
        result = result & 0b00000000000000000000111111111111;

        return result;
    }
    else
    {
        return UINT16_MAX;
    }
}