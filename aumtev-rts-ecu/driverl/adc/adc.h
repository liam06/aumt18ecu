#ifndef _ADC_H
#define _ADC_H

#include "driverl/gpio/gpio.h"
#include "lib/states.h"

#include <stdint.h>

#define ADC_MAXf (float)4095.0f
#define ADC_MAXd (double)4095.0
#define ADC_MAXi (uint16_t)4095.0f


typedef struct {
    unsigned int devIdx;    // ADC IC number
    unsigned int pinIdx;    // AIN number on that IC
} ain_t;

// DECLARATION OF CONNECTED ANALOG INPUTS -------
extern const ain_t AIN0;
extern const ain_t AIN1;
extern const ain_t AIN2;
extern const ain_t AIN3;
extern const ain_t AIN4;
extern const ain_t AIN5;
extern const ain_t AIN6;
extern const ain_t AIN7;
// extern const ain_t AIN8;
// extern const ain_t AIN9;
// extern const ain_t AIN10;
// extern const ain_t AIN11;
// extern const ain_t AIN12;
// extern const ain_t AIN13;
// extern const ain_t AIN14;
// extern const ain_t AIN15;
// ----------------------------------------------

// Digital values for using ADC as GPIO read ----
#define ADC_DLOW_TL  (uint16_t)0    /* Digital low, lower threshold - 0V   */
#define ADC_DLOW_TH  (uint16_t)655  /* Digital low, upper threshold - 0.8V */
#define ADC_DHIGH_TL (uint16_t)1638 /* Digital high, lower threshold - 2V */
#define ADC_DHIGH_TH (uint16_t)4095 /* Digital high, upper threshold - 5V */
// ----------------------------------------------

extern state_t adcState;

extern state_t adcInit();
extern state_t adcUpdate();    // updates the internal store of adc (retrieves all values from ADC banks)
extern uint16_t adcRead(const ain_t *adcDevice);  // Returns from internal store. returns -1 if error
extern int8_t adcReadDigital(const ain_t *adcDevice); // Returns 1 for true, 0 for false, -1 for error

#endif