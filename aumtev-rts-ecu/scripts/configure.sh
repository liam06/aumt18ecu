#!/bin/sh -

# Make sure service has stopped
echo "[Stopping aumtvcu.service]"
systemctl stop aumtvcu.service

# Copy files
echo "[Copying files]"
cp /home/debian/bin/*.sh /root/
cp /home/debian/bin/aumtev /root/aumtev
cp /home/debian/bin/aumtvcu.service /lib/systemd/system/aumtvcu.service

# Enable service
echo "[Enable service]  systemctl daemon-reload"
systemctl daemon-reload
echo "[Enable service]  systemctl reenable aumtvcu.service"
systemctl reenable aumtvcu.service
echo "[Enable service]  systemctl start aumtvcu.service"
systemctl start aumtvcu.service
