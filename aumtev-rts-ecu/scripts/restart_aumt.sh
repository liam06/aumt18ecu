#!/bin/sh -

# Stops the aumtev process and starts it again
# /home/debian/bin/stop.sh
# /home/debian/bin/aumtev

systemctl stop aumtvcu.service
systemctl start aumtvcu.service