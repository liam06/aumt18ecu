#!/bin/sh -

# Simply runs the necessary scripts/programs to start the AUMT embedded system
# Must be run as superuser

mkdir -p /root/logs/     # Create log directory if it doesn't exist
/root/bb_init.sh     # Init BeagleBone devices
/root/aumtev  # Run the RT ECU program
