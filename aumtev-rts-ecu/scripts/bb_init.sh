#!/bin/sh -

# Initialize BeagleBone hardware

# Note #1 this script needs to be run as su
# Note #2 requires overlays to be expressed in /boot/uEnv.txt


############## Init CAN bus #############
echo "[bb_init.sh]  Starting CAN bus"
CAN_BITRATE=1000000
# CAN_BITRATE=250000

# echo BB-CAN1 > /sys/devices/platform/bone_capemgr/slots
modprobe can
modprobe can-dev
modprobe can-raw

#ip link set can0 up type can bitrate $CAN_BITRATE loopback on   # use this line instead for single device debugging (loopback => packets read on same host)
ip link set can0 up type can bitrate $CAN_BITRATE restart-ms 100
ifconfig can0 txqueuelen 1000
ifconfig can0 up

############## Init SPI bus ##############
# echo BB-SPIDEV0 > /sys/devices/platform/bone_capemgr/slots

############## Init GPIO    ##############
# echo "[bb_init.sh]  Starting GPIO"
# # Enable all in use GPIO pins
# echo 48 > /sys/class/gpio/export
# echo 51 > /sys/class/gpio/export
# echo 50 > /sys/class/gpio/export
# echo 20 > /sys/class/gpio/export

# echo 66 > /sys/class/gpio/export
# echo 67 > /sys/class/gpio/export
# echo 69 > /sys/class/gpio/export
# echo 68 > /sys/class/gpio/export
# echo 45 > /sys/class/gpio/export
# echo 44 > /sys/class/gpio/export
# echo 23 > /sys/class/gpio/export
# echo 26 > /sys/class/gpio/export
# echo 47 > /sys/class/gpio/export
# echo 46 > /sys/class/gpio/export
# echo 27 > /sys/class/gpio/export
