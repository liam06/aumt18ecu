#include "bppsDriver.h"

#include <stdio.h>
#include "lib/states.h"
#include "driverl/adc/adc.h"

state_t bppsDriverState = STATE_INIT;


state_t bppsDriverInit()
{
    printf("LOG: driverh/bpps: Initializing throttle sensor\n");
    printf("LOG: driverh/bpps: Operating via CAN bus\n");

    bppsSensorValue = 0.0f;

    bppsDriverSetState(STATE_RUN);
    return bppsDriverGetState();
}

state_t bppsDriverUpdate()
{
    bppsSensorValue = (float)bppsSensorValueRaw/ADC_MAXf;
    return bppsDriverGetState();
}

void bppsDriverSetState(const state_t state)
{
    bppsDriverState = state;
}
state_t bppsDriverGetState()
{
    return bppsDriverState;
}

