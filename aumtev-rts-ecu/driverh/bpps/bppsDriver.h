#ifndef _BPPSDRIVER_H
#define _BPPSDRIVER_H

#include <stdint.h>
#include "lib/states.h"

/*
 * bppsSensorValue: a value from 0 to 1 (0% to 100%) 
 * representing the current brake pedal position value
 */
float bppsSensorValue;
uint16_t bppsSensorValueRaw;


extern state_t bppsDriverInit();
extern state_t bppsDriverUpdate();

extern void bppsDriverSetState(const state_t state);
extern state_t bppsDriverGetState();

#endif