#ifndef _BMSDRIVER_H
#define _BMSDRIVER_H

#include <stdint.h>
#include <stdbool.h>

#include "lib/states.h"

/* TODO:
 * 1. Update new variables (in both raw and true values)
 * 2. Signed values?
 * 3. Extra CAN messages
 * 4. Data conversions (raw->actual value)
 * 5. Update canDriverInterface 
 */

// Define CAN addresses
#define CAN_ID_BMS_PACKVC       0x6B0
#define CAN_ID_BMS_TEMPERATURES 0x6B1
#define CAN_ID_BMS_CELLDATA     0x6B2
#define CAN_ID_BMS_CELLRESIST   0x6B3
#define CAN_ID_BMS_MISC         0x6B4

// Struct containing all broadcast data from bms (raw formats)
typedef struct {
    // Pack voltages and currents
    uint16_t packInstVoltage;
    uint16_t packOpenVoltage;
    uint16_t packCurrent;
    uint16_t averageCurrent;

    // Temperatures
    uint8_t temperatureHigh;
    uint8_t thermistorHigh;
    uint8_t temperatureLow;
    uint8_t thermistorLow;
    uint8_t temperatureInternal;

    // Cell data
    uint16_t opencellHighVoltage;
    uint8_t opencellHighID;
    uint16_t opencellLowVoltage;
    uint8_t opencellLowID;
    uint16_t opencellAvgVoltage;

    // cell resistances (internal resistances)
    uint16_t resistanceCellHigh;
    uint8_t resistanceCellHighID;
    uint16_t resistanceCellLow;
    uint8_t resistanceCellLowID;
    uint16_t resistanceCellAvg;

    // misc
    uint16_t failsafeStatuses;
    uint8_t rollingCounter;
    uint8_t packSOC;
    uint8_t adaptiveSOC;
    uint16_t packDCLMax;
} bmsSensorsRaw_t;
bmsSensorsRaw_t bmsSensorsRaw;

// Struct containing all broadcast data from bms (true data formats)
typedef struct {
    // Pack current
    float packInstVoltage;  // Volts
    float packOpenVoltage;  // Volts
    float packCurrent;      // Amps
    float averageCurrent;   // Amps

    // Temperatures
    float temperatureHigh;  // Degrees
    float thermistorHigh;
    float temperatureLow;
    float thermistorLow;
    float temperatureInternal;

    // Cell data
    float opencellHighVoltage; // Volts
    uint8_t opencellHighID;
    float opencellLowVoltage; // Volts
    uint8_t opencellLowID;
    float opencellAvgVoltage;  // Volts

    // Cell resistances (internal resistances)
    float resistanceCellHigh;       // mOhm
    uint8_t resistanceCellHighID;
    float resistanceCellLow;        // mOhm
    uint8_t resistanceCellLowID;
    float resistanceCellAvg;        // mOhm

    // Misc
    uint16_t failsafeStatuses;
    uint8_t rollingCounter;     // +1 every 100ms
    float packSOC;      // 0-100%
    float adaptiveSOC;  // 0-100%
    float packDCLMax;

    // Calculated
    float packInstPower; // in Watts
} bmsSensors_t;
bmsSensors_t bmsSensors;


extern state_t bmsDriverInit();
extern state_t bmsDriverUpdate();

extern void bmsDriverSetState(const state_t state);
extern state_t bmsDriverGetState();

#endif