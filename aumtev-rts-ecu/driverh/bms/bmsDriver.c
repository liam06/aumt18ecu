#include "bmsDriver.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "lib/states.h"
#include "lib/bitsplit.h"
#include "driverl/can/can.h"

state_t bmsDriverState = STATE_INIT;

state_t bmsDriverInit()
{
    printf("LOG: driverh/bms: Initializaing BMS\n");

    // TODO
    bmsDriverSetState(STATE_RUN);
    return bmsDriverGetState();
}

state_t bmsDriverUpdate()
{
    if (bmsDriverGetState() == STATE_RUN)
    {
        // Raw to actual sensor value conversions:

        // Pack voltages and currents
        bmsSensors.packInstVoltage = (float)bmsSensorsRaw.packInstVoltage * 0.1f;
        bmsSensors.packOpenVoltage = (float)bmsSensorsRaw.packOpenVoltage * 0.1f;
        bmsSensors.packCurrent = (float)(int16_t)bmsSensorsRaw.packCurrent * 0.1f;
        bmsSensors.averageCurrent = (float)(int16_t)bmsSensorsRaw.averageCurrent * 0.1f;

        // Temperatures
        bmsSensors.temperatureHigh = (float)(int16_t)bmsSensorsRaw.temperatureHigh;
        bmsSensors.thermistorHigh = (float)bmsSensorsRaw.thermistorHigh;
        bmsSensors.temperatureLow = (float)(int16_t)bmsSensorsRaw.temperatureLow;
        bmsSensors.thermistorLow = (float)bmsSensorsRaw.thermistorLow;
        bmsSensors.temperatureInternal = (float)(int16_t)bmsSensorsRaw.temperatureInternal;
        
        // Cell data
        bmsSensors.opencellHighVoltage = (float)bmsSensorsRaw.opencellHighVoltage * 0.0001f;
        bmsSensors.opencellLowVoltage = (float)bmsSensorsRaw.opencellLowVoltage * 0.0001f;
        bmsSensors.opencellAvgVoltage = (float)bmsSensorsRaw.opencellAvgVoltage * 0.0001f;
        bmsSensors.opencellHighID = bmsSensorsRaw.opencellHighID;
        bmsSensors.opencellLowID = bmsSensorsRaw.opencellLowID;

        // Cell resistances (internal resistances)
        bmsSensors.resistanceCellHigh = (float)bmsSensorsRaw.resistanceCellHigh * 0.01f;
        bmsSensors.resistanceCellLow = (float)bmsSensorsRaw.resistanceCellLow * 0.01f;
        bmsSensors.resistanceCellAvg = (float)bmsSensorsRaw.resistanceCellAvg * 0.01f;
        bmsSensors.resistanceCellHighID = bmsSensorsRaw.resistanceCellHighID;
        bmsSensors.resistanceCellLowID = bmsSensorsRaw.resistanceCellLowID;

        // Misc
        bmsSensors.failsafeStatuses = bmsSensorsRaw.failsafeStatuses;
        bmsSensors.rollingCounter = bmsSensorsRaw.rollingCounter;
        bmsSensors.packSOC = bmsSensorsRaw.packSOC * 0.5f;
        bmsSensors.adaptiveSOC = bmsSensorsRaw.adaptiveSOC * 0.5f;
        bmsSensors.packDCLMax = bmsSensorsRaw.packDCLMax;

        // Calculated
        bmsSensors.packInstPower = bmsSensors.packInstVoltage * bmsSensors.packCurrent;
    }

    return bmsDriverGetState();
}

void bmsDriverSetState(const state_t state)
{
    bmsDriverState = state;
}
state_t bmsDriverGetState()
{
    return bmsDriverState;
}
