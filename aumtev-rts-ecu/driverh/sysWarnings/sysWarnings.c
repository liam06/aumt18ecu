#include "sysWarnings.h"

#include <stdio.h>
#include <stdbool.h>

#include "driverl/adc/adc.h"
#include "lib/states.h"

// Define pins that warnings are connected to
const ain_t* pinIMDin = &AIN3;   // BAIN7
const ain_t* pinBMSin = &AIN2;   // BAIN8
const ain_t* pinBSPDin = &AIN0;  // BAIN4
const ain_t* pinPDOCin = &AIN1;  // BAIN3

// Latching values
typedef struct {
    bool value;
    uint16_t counter;
} sysWarningLatch_t;
sysWarningLatch_t SYS_WARNING_LATCH_INIT = {.value = false, .counter = 0};

struct {
    sysWarningLatch_t errorIMD;
    sysWarningLatch_t errorBMS;
    sysWarningLatch_t errorBSPD;
    sysWarningLatch_t errorPDOC;
    sysWarningLatch_t errorECU;
} sysWarningLatches;

// Send the signal high unless the sample is low for the number of samples
void latchHigh(sysWarningLatch_t* main, const bool sample, const uint16_t samples);
// Once the signal goes high, it never goes low again
void latchHold(sysWarningLatch_t* main, const bool sample);



state_t sysWarningsInit()
{
    printf("LOG: driverh/sysWarning: Initializing system warning loader\n");

    // init all to false
    sysWarningStatus.errorIMD = false;
    sysWarningStatus.errorBMS = false;
    sysWarningStatus.errorBSPD = false;
    sysWarningStatus.errorPDOC = false;
    sysWarningStatus.errorECU = false;
    
    sysWarningLatches.errorIMD = SYS_WARNING_LATCH_INIT;
    sysWarningLatches.errorBMS = SYS_WARNING_LATCH_INIT;
    sysWarningLatches.errorBSPD = SYS_WARNING_LATCH_INIT;
    sysWarningLatches.errorPDOC = SYS_WARNING_LATCH_INIT;
    sysWarningLatches.errorECU = SYS_WARNING_LATCH_INIT;

    return STATE_RUN;
}

state_t sysWarningsUpdate()
{
    // set error if unable to read or actual error
    // i.e. false if adcGpioRead=0, true otherwise
    sysWarningStatus.errorIMD = (adcReadDigital(pinIMDin) == 0) ? true : false;
    sysWarningStatus.errorBMS = (adcReadDigital(pinBMSin) == 0) ? true : false;
    sysWarningStatus.errorBSPD = ((adcReadDigital(pinBSPDin) == 0) ? true : false) || sysWarningStatus.errorBSPD;
    sysWarningStatus.errorPDOC = (adcReadDigital(pinPDOCin) == 0) ? true : false;
    // sysWarningStatus.errorECU set externally

    // Perform latching
    latchHold(&sysWarningLatches.errorIMD, sysWarningStatus.errorIMD);
    latchHigh(&sysWarningLatches.errorBMS, sysWarningStatus.errorBMS, 50);
    latchHigh(&sysWarningLatches.errorBSPD, sysWarningStatus.errorBSPD, 50);
    latchHigh(&sysWarningLatches.errorPDOC, sysWarningStatus.errorPDOC, 50);
    latchHigh(&sysWarningLatches.errorECU, sysWarningStatus.errorECU, 50);

    // Copy out latching
    sysWarningStatus.errorIMD = sysWarningLatches.errorIMD.value;
    sysWarningStatus.errorBMS = sysWarningLatches.errorBMS.value;
    sysWarningStatus.errorBSPD = sysWarningLatches.errorBSPD.value;
    sysWarningStatus.errorPDOC = sysWarningLatches.errorPDOC.value;
    sysWarningStatus.errorECU = sysWarningLatches.errorECU.value;

    return STATE_RUN;
}

void latchHigh(sysWarningLatch_t* main, const bool sample, const uint16_t samples)
{
    if (sample == true)
    {
        main->counter = 0;
        main->value = true;
    }
    else
    {
        main->counter++;
        if (main->counter >= samples)
        {
            main->counter = 0;
            main->value = false;
        }
    }
}

void latchHold(sysWarningLatch_t* main, const bool sample)
{
    // either on now, or was on before
    main->value = main->value || sample;
}