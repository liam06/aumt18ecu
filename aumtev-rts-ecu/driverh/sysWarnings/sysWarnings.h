#ifndef _SYSWARNINGS_H
#define _SYSWARNINGS_H

#include <stdbool.h>
#include "lib/states.h"

typedef struct {
    bool errorIMD;
    bool errorBMS;
    bool errorBSPD;
    bool errorPDOC;
    bool errorECU;
} sysWarning_t;
sysWarning_t sysWarningStatus;

state_t sysWarningsInit();
state_t sysWarningsUpdate();

#endif