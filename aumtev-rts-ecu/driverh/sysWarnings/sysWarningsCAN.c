#include "sysWarnings.h"

#include <stdio.h>
#include <stdbool.h>

#include "lib/states.h"


state_t sysWarningsInit()
{
    printf("LOG: driverh/sysWarning: Initializing system warning loader (via CAN)\n");

    // init all to false
    sysWarningStatus.errorIMD = false;
    sysWarningStatus.errorBMS = false;
    sysWarningStatus.errorBSPD = false;
    sysWarningStatus.errorPDOC = false;

    return STATE_RUN;
}

state_t sysWarningsUpdate()
{
    // Nothing required (can interface loads data)

    return STATE_RUN;
}