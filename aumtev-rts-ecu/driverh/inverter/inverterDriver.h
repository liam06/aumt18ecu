#ifndef _INVERTERDRIVER_H
#define _INVERTERDRIVER_H

#include <stdint.h>
#include <stdbool.h>

#include "lib/states.h"

// Define CAN addresses
// Output
#define CAN_ID_INVERTER_CONTROL       0x0C0
#define CAN_ID_INVERTER_PARAM_WRITE   0x0C1
#define CAN_ID_INVERTER_PARAM_READ    0x0C2
// Sensors
#define CAN_ID_INVERTER_TEMPERATURES1 0x0A0
#define CAN_ID_INVERTER_TEMPERATURES2 0x0A1
#define CAN_ID_INVERTER_TEMPERATURES3 0x0A2
#define CAN_ID_INVERTER_ANALOGIN      0x0A3
#define CAN_ID_INVERTER_DIGITALIN     0x0A4
#define CAN_ID_INVERTER_MOTORPOSITION 0x0A5
#define CAN_ID_INVERTER_CURRENT       0x0A6
#define CAN_ID_INVERTER_VOLTAGE       0x0A7
#define CAN_ID_INVERTER_FLUX          0x0A8
#define CAN_ID_INVERTER_INTERNALVOLT  0x0A9
#define CAN_ID_INVERTER_INTERNALSTATE 0x0AA
#define CAN_ID_INVERTER_FAULTCODES    0x0AB
#define CAN_ID_INVERTER_TORQUETIMER   0x0AC
#define CAN_ID_INVERTER_MODFLUXWEAK   0x0AD
#define CAN_ID_INVERTER_FIRMWARE      0x0AE

// Struct containing all broadcast data from inverter (raw formats)
typedef struct {
    // Temperatures #1
    int16_t temperatureModuleA; 
    int16_t temperatureModuleB; 
    int16_t temperatureModuleC; 
    int16_t temperatureGateDriverBoard;
    // Temperatures #2
    int16_t temperatureBoard;
    int16_t temperatureRTD1;
    int16_t temperatureRTD2;
    int16_t temperatureRTD3;
    // Temperatures #3 & Torque Shudder
    int16_t temperatureRTD4;
    int16_t temperatureRTD5;
    int16_t temperatureMotor;
    int16_t torqueShudder;
    // Analog In voltages
    int16_t analogInput1;  // (note only 10 bits used)
    int16_t analogInput2;
    int16_t analogInput3;
    int16_t analogInput4;
    int16_t analogInput5;
    int16_t analogInput6;
    // Digital Input Status
    uint8_t digitalInput1; 
    uint8_t digitalInput2; 
    uint8_t digitalInput3; 
    uint8_t digitalInput4; 
    uint8_t digitalInput5; 
    uint8_t digitalInput6; 
    uint8_t digitalInput7; 
    uint8_t digitalInput8; 
    // Motor position information
    int16_t motorAngle;
    int16_t motorSpeed;
    int16_t electricalFrequency;
    int16_t deltaResolver; 
    // Current information
    int16_t currentPhaseA;
    int16_t currentPhaseB;
    int16_t currentPhaseC;
    int16_t currentDCBus;
    // Voltage information
    int16_t voltageDCBus;
    int16_t voltateOutput;
    int16_t voltageVAB_Vd;
    int16_t voltageVBC_Vq;
    // Flux information
    int16_t fluxCommand;
    int16_t fluxFeedback;
    int16_t feedbackId;
    int16_t feedbackIq;
    // Internal voltages
    int16_t voltageReference1_5;
    int16_t voltageReference2_5;
    int16_t voltageReference5_0;
    int16_t voltageReference12_0;
    // Internal States
    uint16_t stateVSM;
    uint8_t stateInverter;
    uint8_t stateRelay;
    uint8_t stateInverterRunMode; // (note only 1 bit used)
    uint8_t stateInverterActiveDischarge;
    uint8_t stateInverterCommandMode; 
    uint8_t stateInverterEnable; // (note only 1 bit used)
    uint8_t stateInverterEnableLockout;
    uint8_t directionCommand;
    // Fault codes
    uint16_t faultPOSTLo;
    uint16_t faultPOSTHi;
    uint16_t faultRunLo;
    uint16_t faultRunHi;
    // Torque and timer information
    int16_t torqueCommanded;
    int16_t torqueFeedback;
    uint32_t timerPowerOn;
    // Modulation Index & Flux Weaking Output information
    uint16_t modulationIndex;
    int16_t fluxWeakeningOutput;
    int16_t commandId;
    int16_t commandIq;
    // Firmware information
    uint16_t firmwareEEPROMVersion;
    uint16_t firmwareSoftwareVersion;
    uint16_t firmwareDateCodeMMDD;
    uint16_t firmwareDateCodeYYYY;
} inverterSensorsRaw_t;
inverterSensorsRaw_t inverterSensorsRaw;

// Struct containing all broadcast data from inverter (true data formats)
typedef struct {
    // Temperatures #1
    float temperatureModuleA; // Degrees
    float temperatureModuleB; 
    float temperatureModuleC; 
    float temperatureGateDriverBoard;
    // Temperatures #2
    float temperatureBoard;
    float temperatureRTD1;
    float temperatureRTD2;
    float temperatureRTD3;
    // Temperatures #3 & Torque Shudder
    float temperatureRTD4;
    float temperatureRTD5;
    float temperatureMotor;
    float torqueShudder; // N.m
    // Analog In voltages
    float analogInput1;  // Volts
    float analogInput2;
    float analogInput3;
    float analogInput4;
    float analogInput5;
    float analogInput6;
    // Digital Input Status
    bool digitalInput1; 
    bool digitalInput2; 
    bool digitalInput3; 
    bool digitalInput4; 
    bool digitalInput5; 
    bool digitalInput6; 
    bool digitalInput7; 
    bool digitalInput8; 
    // Motor position information
    float motorAngle; // degrees
    int16_t motorSpeed; // RPM
    float electricalFrequency; // Hz
    float deltaResolver; // Degrees
    // Current information
    float currentPhaseA; // Amps
    float currentPhaseB;
    float currentPhaseC;
    float currentDCBus;
    // Voltage information
    float voltageDCBus; // Volts
    float voltateOutput;
    float voltageVAB_Vd;
    float voltageVBC_Vq;
    // Flux information
    float fluxCommand;  // Webers
    float fluxFeedback; // Webers
    float feedbackId; // Amps
    float feedbackIq;
    // Internal voltages
    float voltageReference1_5; // Volts
    float voltageReference2_5;
    float voltageReference5_0;
    float voltageReference12_0;
    // Internal States
    uint16_t stateVSM;
    uint8_t stateInverter;
    uint8_t stateRelay;
    uint8_t stateInverterRunMode; // (note only 1 bit used)
    uint8_t stateInverterActiveDischarge;
    uint8_t stateInverterCommandMode; 
    uint8_t stateInverterEnable; // (note only 1 bit used)
    uint8_t stateInverterEnableLockout;
    uint8_t directionCommand;
    // Fault codes
    uint16_t faultPOSTLo;
    uint16_t faultPOSTHi;
    uint16_t faultRunLo;
    uint16_t faultRunHi;
    // Torque and timer information
    float torqueCommanded; // N.m
    float torqueFeedback;
    double timerPowerOn; // Seconds
    // Modulation Index & Flux Weaking Output information
    float modulationIndex; // Per-unit value
    float fluxWeakeningOutput; // Amps
    float commandId; // Amps
    float commandIq; // Amps
    // Firmware information
    uint16_t firmwareEEPROMVersion;    // ??
    uint16_t firmwareSoftwareVersion;  // ??
    uint16_t firmwareDateCodeMMDD;     // ??
    uint16_t firmwareDateCodeYYYY;     // ??
} inverterSensors_t;
inverterSensors_t inverterSensors;

// Struct containing command message data 
typedef struct {
    float commandTorque;
    uint16_t commandSpeed; 
    uint8_t commandDirection;
    uint8_t inverterEnable; // only 1 bit used
    uint8_t inverterDischarge; // only 1 bit used
    uint8_t speedModeEnable; // only 1 bit used
    uint16_t commandTorqueLimit; 
} inverterCommand_t;

// Driver directions
typedef enum {
    inverterDirectionReverse = 0,
    inverterDirectionForward = 1
} inverterDirection_t;

// Parameter read write structs
typedef struct {
    uint16_t paramAddress;
    bool write;  // true if writing, false if reading
    uint16_t data;
} inverterParamIn_t;   // input to inverter (output from control)
typedef struct {
    bool newMsg;
    uint16_t paramAddress;
    bool writeSuccess;
    uint16_t data;
} inverterParamOut_t;  // output from inverter (input to control)
inverterParamOut_t inverterParamOut;

extern state_t inverterDriverInit();
extern state_t inverterDriverUpdate();

// Creates an instruction for the inverter with the given command paramters
extern void inverterDriverSendCommand(const inverterCommand_t *command);
// Sends a torque drive instruction
extern void inverterDriverTorqueRequest(const float torque, const inverterDirection_t direction);
// Enables or disables the inverter
extern void inverterDriverSetEnabled(const bool inverterEnabled);
// Sends a parameter message to the inverter
extern void inverterDriverWriteParam(const inverterParamIn_t* params);

// Param setting functions
extern void inverterDriverClearFaults();

extern void inverterDriverSetState(const state_t state);
extern state_t inverterDriverGetState();

#endif