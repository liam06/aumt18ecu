#include "inverterDriver.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "lib/states.h"
#include "lib/bitsplit.h"
#include "driverl/can/can.h"

state_t inverterDriverState = STATE_INIT;

state_t inverterDriverInit()
{
    printf("LOG: driverh/inverter: Initializing inverter\n");

    inverterParamOut.newMsg = false;

    inverterDriverSetState(STATE_RUN);
    return inverterDriverGetState();
}

state_t inverterDriverUpdate()
{
    if (inverterDriverGetState() == STATE_RUN)
    {
        // Raw to actual sensor value conversions:
        // Temperatures #1
        inverterSensors.temperatureModuleA = (float)inverterSensorsRaw.temperatureModuleA / 10;
        inverterSensors.temperatureModuleB = (float)inverterSensorsRaw.temperatureModuleB / 10;
        inverterSensors.temperatureModuleC = (float)inverterSensorsRaw.temperatureModuleC / 10;
        inverterSensors.temperatureGateDriverBoard = (float)inverterSensorsRaw.temperatureGateDriverBoard / 10;
        // Temperatures #2
        inverterSensors.temperatureBoard = (float)inverterSensorsRaw.temperatureBoard / 10;
        inverterSensors.temperatureRTD1 = (float)inverterSensorsRaw.temperatureRTD1 / 10;
        inverterSensors.temperatureRTD2 = (float)inverterSensorsRaw.temperatureRTD2 / 10;
        inverterSensors.temperatureRTD3 = (float)inverterSensorsRaw.temperatureRTD3 / 10;
        // Temperatures #3 & Torque Shudder
        inverterSensors.temperatureRTD4 = (float)inverterSensorsRaw.temperatureRTD4 / 10;
        inverterSensors.temperatureRTD5 = (float)inverterSensorsRaw.temperatureRTD5 / 10;
        inverterSensors.temperatureMotor = (float)inverterSensorsRaw.temperatureMotor / 10;
        inverterSensors.torqueShudder = (float)inverterSensorsRaw.torqueShudder / 10;
        // Analog In voltages
        inverterSensors.analogInput1 = (float)inverterSensorsRaw.analogInput1 / 100;
        inverterSensors.analogInput2 = (float)inverterSensorsRaw.analogInput2 / 100;
        inverterSensors.analogInput3 = (float)inverterSensorsRaw.analogInput3 / 100;
        inverterSensors.analogInput4 = (float)inverterSensorsRaw.analogInput4 / 100;
        inverterSensors.analogInput5 = (float)inverterSensorsRaw.analogInput5 / 100;
        inverterSensors.analogInput6 = (float)inverterSensorsRaw.analogInput6 / 100;
        // Digital Input Status
        inverterSensors.digitalInput1 = (bool)inverterSensorsRaw.digitalInput1;
        inverterSensors.digitalInput2 = (bool)inverterSensorsRaw.digitalInput2;
        inverterSensors.digitalInput3 = (bool)inverterSensorsRaw.digitalInput3;
        inverterSensors.digitalInput4 = (bool)inverterSensorsRaw.digitalInput4;
        inverterSensors.digitalInput5 = (bool)inverterSensorsRaw.digitalInput5;
        inverterSensors.digitalInput6 = (bool)inverterSensorsRaw.digitalInput6;
        inverterSensors.digitalInput7 = (bool)inverterSensorsRaw.digitalInput7;
        inverterSensors.digitalInput8 = (bool)inverterSensorsRaw.digitalInput8;
        // Motor position information
        inverterSensors.motorAngle = (float)inverterSensorsRaw.motorAngle / 10;
        inverterSensors.motorSpeed = (int16_t)inverterSensorsRaw.motorSpeed;
        inverterSensors.electricalFrequency = (float)inverterSensorsRaw.electricalFrequency / 10;
        inverterSensors.deltaResolver = (float)inverterSensorsRaw.deltaResolver / 10;
        // Current information
        inverterSensors.currentPhaseA = (float)inverterSensorsRaw.currentPhaseA / 10;
        inverterSensors.currentPhaseB = (float)inverterSensorsRaw.currentPhaseB / 10;
        inverterSensors.currentPhaseC = (float)inverterSensorsRaw.currentPhaseC / 10;
        inverterSensors.currentDCBus = (float)inverterSensorsRaw.currentDCBus / 10;
        // Voltage information
        inverterSensors.voltageDCBus = (float)inverterSensorsRaw.voltageDCBus / 10;
        inverterSensors.voltateOutput = (float)inverterSensorsRaw.voltateOutput / 10;
        inverterSensors.voltageVAB_Vd = (float)inverterSensorsRaw.voltageVAB_Vd / 10;
        inverterSensors.voltageVBC_Vq = (float)inverterSensorsRaw.voltageVBC_Vq / 10;
        // Flux information
        inverterSensors.fluxCommand = (float)inverterSensorsRaw.fluxCommand / 1000;
        inverterSensors.fluxFeedback = (float)inverterSensorsRaw.fluxFeedback / 1000;
        inverterSensors.feedbackId = (float)inverterSensorsRaw.feedbackId / 10;
        inverterSensors.feedbackId = (float)inverterSensorsRaw.feedbackId / 10;
        // Internal voltages
        inverterSensors.voltageReference1_5 = (float)inverterSensorsRaw.voltageReference1_5 / 100;
        inverterSensors.voltageReference2_5 = (float)inverterSensorsRaw.voltageReference2_5 / 100;
        inverterSensors.voltageReference5_0 = (float)inverterSensorsRaw.voltageReference5_0 / 100;
        inverterSensors.voltageReference12_0 = (float)inverterSensorsRaw.voltageReference12_0 / 100;
        // Internal States
        inverterSensors.stateVSM = inverterSensorsRaw.stateVSM;
        inverterSensors.stateInverter = inverterSensorsRaw.stateInverter;
        inverterSensors.stateRelay = inverterSensorsRaw.stateRelay;
        inverterSensors.stateInverterRunMode = inverterSensorsRaw.stateInverterRunMode;
        inverterSensors.stateInverterActiveDischarge = inverterSensorsRaw.stateInverterActiveDischarge;
        inverterSensors.stateInverterCommandMode = inverterSensorsRaw.stateInverterCommandMode;
        inverterSensors.stateInverterEnable = inverterSensorsRaw.stateInverterEnable;
        inverterSensors.stateInverterEnableLockout = inverterSensorsRaw.stateInverterEnableLockout;
        inverterSensors.directionCommand = inverterSensorsRaw.directionCommand;
        // Fault codes
        inverterSensors.faultPOSTLo = inverterSensorsRaw.faultPOSTLo;
        inverterSensors.faultPOSTHi = inverterSensorsRaw.faultPOSTHi;
        inverterSensors.faultRunLo = inverterSensorsRaw.faultRunLo;
        inverterSensors.faultRunHi = inverterSensorsRaw.faultRunHi;
        // Torque and timer information
        inverterSensors.torqueCommanded = (float)inverterSensorsRaw.torqueCommanded / 10;
        inverterSensors.torqueFeedback = (float)inverterSensorsRaw.torqueFeedback / 10;
        inverterSensors.timerPowerOn = (double)inverterSensorsRaw.timerPowerOn * 0.003;
        // Modulation Index & Flux Weaking Output information
        inverterSensors.modulationIndex = (float)inverterSensorsRaw.modulationIndex / 100;
        inverterSensors.fluxWeakeningOutput = (float)inverterSensorsRaw.fluxWeakeningOutput / 10;
        inverterSensors.commandId = (float)inverterSensorsRaw.commandId / 10;
        inverterSensors.commandIq = (float)inverterSensorsRaw.commandIq / 10;
        // Firmware information (??)
        inverterSensors.firmwareEEPROMVersion = inverterSensorsRaw.firmwareEEPROMVersion;
        inverterSensors.firmwareSoftwareVersion = inverterSensorsRaw.firmwareSoftwareVersion;
        inverterSensors.firmwareDateCodeMMDD = inverterSensorsRaw.firmwareDateCodeMMDD;
        inverterSensors.firmwareDateCodeYYYY = inverterSensorsRaw.firmwareDateCodeYYYY;
        
    }

    return inverterDriverGetState();
}

// Creates an instruction for the inverter with the given command paramters
void inverterDriverSendCommand(const inverterCommand_t *command)
{
    if (inverterDriverGetState() == STATE_RUN)
    {
        uint8_t canId = CAN_ID_INVERTER_CONTROL;
        uint8_t canData[8] = {0};

        // Fill in the can data fields
        splitIntToBytes((int16_t)(command->commandTorque*10), canData+0, 2, endianLittle);
        splitIntToBytes(command->commandSpeed, canData+2, 2, endianLittle);
        canData[4] = command->commandDirection;
        canData[5] |= (command->inverterEnable & 0b00000001) << 0;
        canData[5] |= (command->inverterDischarge & 0b00000001) << 1;
        canData[5] |= (command->speedModeEnable & 0b00000001) << 2;
        splitIntToBytes(command->commandTorqueLimit*10, canData+6, 2, endianLittle);

        canWrite(canId, 8, canData);
    }
}

// Sends a torque drive instruction
void inverterDriverTorqueRequest(const float torque, const inverterDirection_t direction)
{
    if (inverterDriverGetState() == STATE_RUN)
    {
        inverterCommand_t commandMessage;

        commandMessage.commandTorque = torque;
        commandMessage.commandSpeed = 0;  // TEMP?
        commandMessage.commandDirection = direction;
        commandMessage.inverterEnable = 1;
        commandMessage.inverterDischarge = 0;
        commandMessage.speedModeEnable = 0;
        commandMessage.commandTorqueLimit = 0; // default torque limits

        inverterDriverSendCommand(&commandMessage);
    }
}

// Enables or disables the inverter
void inverterDriverSetEnabled(const bool inverterEnabled)
{
    if (inverterDriverGetState() == STATE_RUN)
    {
        inverterCommand_t commandMessage;

        commandMessage.commandTorque = 0;
        commandMessage.commandSpeed = 0;  // TEMP?
        commandMessage.commandDirection = 1;
        commandMessage.inverterEnable = (inverterEnabled == true) ? 1 : 0;
        commandMessage.inverterDischarge = 0;
        commandMessage.speedModeEnable = 0;
        commandMessage.commandTorqueLimit = 0; // default torque limits

        inverterDriverSendCommand(&commandMessage);
    }
}

void inverterDriverWriteParam(const inverterParamIn_t* params)
{
    if (inverterDriverGetState() == STATE_RUN)
    {
        uint8_t canId = CAN_ID_INVERTER_PARAM_WRITE;
        uint8_t canData[8] = {0};

        // Fill in the can data fields
        splitIntToBytes(params->paramAddress, canData+0, 2, endianLittle);
        canData[2] = (params->write == true) ? 1 : 0;
        splitIntToBytes(params->data, canData+4, 2, endianLittle);

        canWrite(canId, 8, canData);
    }
}

void inverterDriverClearFaults()
{
    // send a clear faults command
    inverterParamIn_t clearParams;
    clearParams.paramAddress = 20;
    clearParams.write = true;
    clearParams.data = 0;
    inverterDriverWriteParam(&clearParams);
}

void inverterDriverSetState(const state_t state)
{
    inverterDriverState = state;
}
state_t inverterDriverGetState()
{
    return inverterDriverState;
}
