#include "appsADriver.h"

#include <stdio.h>
#include "lib/states.h"
#include "driverl/adc/adc.h"
#include "appsACalibration.h"

state_t appsADriverState = STATE_INIT;


state_t appsADriverInit()
{
    printf("LOG: driverh/appsA: Initializing throttle sensor\n");
    printf("LOG: driverh/appsA: Operating via CAN bus\n");

    appsASensorValue = 0.0;

    appsADriverSetState(STATE_RUN);
    return appsADriverGetState();
}

state_t appsADriverUpdate()
{
    
    // calculate the scaled sensor value
    // if (appsASensorValueRaw < appsAPosition0)
    //     appsASensorValue = 0.0f;
    // else if (appsASensorValueRaw > appsAPosition1)
    //     appsASensorValue = 1.0f;
    // else
    // {
        appsASensorValue = ( (float)appsASensorValueRaw - appsAPosition0 ) / (appsAPosition1-appsAPosition0);
        // appsASensorValue = ( -(float)appsASensorValueRaw + appsAPosition0 ) / (appsAPosition0-appsAPosition1);
        
    // }
    
    return appsADriverGetState();
}

void appsADriverSetState(const state_t state)
{
    appsADriverState = state;
}
state_t appsADriverGetState()
{
    return appsADriverState;
}

