#ifndef _APPSADRIVER_H
#define _APPSADRIVER_H

#include <stdint.h>
#include "lib/states.h"

/*
 * appsASensorValue: a value from 0 to 1 (0% to 100%) 
 * representing the current throttle value.
 */
float appsASensorValue;
uint16_t appsASensorValueRaw;  // raw data from ADC


extern state_t appsADriverInit();
extern state_t appsADriverUpdate();

extern void appsADriverSetState(const state_t state);
extern state_t appsADriverGetState();

#endif