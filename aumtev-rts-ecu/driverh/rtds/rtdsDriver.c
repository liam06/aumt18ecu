#include "rtdsDriver.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "lib/states.h"
#include "driverl/gpio/gpio.h"

uint16_t soundcount;
bool sounding;

const pin_t* pinRtds = &P8_16;

state_t rtdsDriverInit()
{
    printf("LOG: driverh/rtds: Initializing RTDS\n");

    sounding = false;
    soundcount = 0;

    return STATE_RUN;
}

state_t rtdsDriverUpdate()
{
    gpioWrite(pinRtds, sounding);

    if (sounding)
    {
        // counter
        if (++soundcount == RTDS_LEN_TICKS)
        {
            soundcount = 0;
            sounding = false;
        }
    }
    
    return STATE_RUN;
}

void rtdsDriverPlay()
{
    sounding = true;
    soundcount = 0;
}