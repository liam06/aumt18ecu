#ifndef _RTDS_H
#define _RTDS_H

#include <stdint.h>
#include "lib/states.h"

#define RTDS_LEN_TICKS (uint16_t)300 /* length of RTDS play sound */

extern state_t rtdsDriverInit();
extern state_t rtdsDriverUpdate();

extern void rtdsDriverPlay();  // starts the RTDS

#endif