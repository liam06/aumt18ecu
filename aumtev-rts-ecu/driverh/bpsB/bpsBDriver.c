#include "bpsBDriver.h"

#include <stdio.h>
#include <stdint.h>
#include "lib/states.h"
#include "driverl/adc/adc.h"

state_t bpsBDriverState = STATE_INIT;


state_t bpsBDriverInit()
{
    printf("LOG: driverh/bpsB: Initializing throttle sensor\n");

    bpsBSensorValue = 0.0f;
    bpsBSensorValueRaw = 0;

    bpsBDriverSetState(STATE_RUN);

    return bpsBDriverGetState();
}

state_t bpsBDriverUpdate()
{
    bpsBSensorValueRaw = adcRead(&AIN3);
    bpsBSensorValue = (float)bpsBSensorValueRaw/ADC_MAXf;

    return bpsBDriverGetState();
}

void bpsBDriverSetState(const state_t state)
{
    bpsBDriverState = state;
}
state_t bpsBDriverGetState()
{
    return bpsBDriverState;
}

