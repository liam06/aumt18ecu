#include "bpsBDriver.h"

#include <stdio.h>
#include "lib/states.h"
#include "driverl/adc/adc.h"

state_t bpsBDriverState = STATE_INIT;


state_t bpsBDriverInit()
{
    printf("LOG: driverh/bpsB: Initializing throttle sensor\n");
    printf("LOG: driverh/bpsB: Operating via CAN bus\n");

    bpsBSensorValue = 0.0f;

    bpsBDriverSetState(STATE_RUN);
    return bpsBDriverGetState();
}

state_t bpsBDriverUpdate()
{
    bpsBSensorValue = (float)bpsBSensorValueRaw/ADC_MAXf;
    return bpsBDriverGetState();
}

void bpsBDriverSetState(const state_t state)
{
    bpsBDriverState = state;
}
state_t bpsBDriverGetState()
{
    return bpsBDriverState;
}

