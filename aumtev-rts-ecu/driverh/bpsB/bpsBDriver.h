#ifndef _BPSBDRIVER_H
#define _BPSBDRIVER_H

#include <stdint.h>
#include "lib/states.h"

/*
 * bpsBSensorValue: a value from 0 to 1 (0% to 100%) 
 * representing the current brake position value.
 */
float bpsBSensorValue;
uint16_t bpsBSensorValueRaw;


extern state_t bpsBDriverInit();
extern state_t bpsBDriverUpdate();

extern void bpsBDriverSetState(const state_t state);
extern state_t bpsBDriverGetState();

#endif