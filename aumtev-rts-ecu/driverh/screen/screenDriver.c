#include "screenDriver.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "driverl/uart/uart.h"
#include "driverh/inverter/inverterDriver.h"
#include "lib/states.h"

state_t screenDriverInit()
{
    printf("LOG: driverh/screen: Initializing screen\n");

    // Nothing to do
    return STATE_RUN;
}

state_t screenDriverUpdate()
{
    // acquire data
    int16_t batteryTemp = 25; // Degrees    UPDATE
    int16_t inverterTemp = 0; // Degrees
    int16_t batteryCharge = 42; // Percent  UPDATE
    int16_t motorTemp = (int16_t)inverterSensors.temperatureMotor;

    float inverterTempf = 0.0f;
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureModuleA);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureModuleB);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureModuleC);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureGateDriverBoard);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureBoard);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureRTD1);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureRTD2);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureRTD3);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureRTD4);
    inverterTempf = fmaxf(inverterTempf, inverterSensors.temperatureRTD5);
    inverterTemp = (int16_t)inverterTempf;

    // send data
    screenDriverSetVariableInt("n0", "val", batteryTemp);
    screenDriverSetVariableInt("n1", "val", inverterTemp);
    screenDriverSetVariableInt("n2", "val", batteryCharge);
    screenDriverSetVariableInt("n3", "val", motorTemp);

    return STATE_RUN;
}

int screenDriverSetVariableStr(const char* objName, const char* varName, const char* value)
{
    char buf[64];

    size_t l1 = strlen(objName);
    size_t l2 = strlen(varName);
    size_t l3 = strlen(value);
    if (l1+l2+l3+1 > 64)
    {
        printf("ERROR: driverh/screenDriver: Data too big for string buffer\n");
        return -1;
    }

    sprintf(buf, "%s.%s=\"%s\"", objName, varName, value);

    uartWrite(4, buf, strlen(buf)+1);

    uint8_t endNum = 0xFF;
    uartWrite(4, (void*)&endNum, sizeof(uint8_t));
    uartWrite(4, (void*)&endNum, sizeof(uint8_t));
    uartWrite(4, (void*)&endNum, sizeof(uint8_t));

    return 0;
}

int screenDriverSetVariableInt(const char* objName, const char* varName, const int value)
{
    char buf[64];

    size_t l1 = strlen(objName);
    size_t l2 = strlen(varName);
    size_t l3 = 6;  // max num characters for an int
    if (l1+l2+l3+1 > 64)
    {
        printf("ERROR: driverh/screenDriver: Data too big for string buffer\n");
        return -1;
    }

    sprintf(buf, "%s.%s=%d", objName, varName, value);

    uartWrite(4, buf, strlen(buf)+1);

    uint8_t endNum = 0xFF;
    uartWrite(4, (void*)&endNum, sizeof(uint8_t));
    uartWrite(4, (void*)&endNum, sizeof(uint8_t));
    uartWrite(4, (void*)&endNum, sizeof(uint8_t));

    return 0;
}
