#ifndef _SCREENDRIVER_H
#define _SCREENDRIVER_H

#include "lib/states.h"

extern state_t screenDriverInit();
extern state_t screenDriverUpdate();

// Sets objName.varName = value in the screen, where value is a string
extern int screenDriverSetVariableStr(const char* objName, const char* varName, const char* value);
// Sets objName.varName = value in the screen, where value is an int
extern int screenDriverSetVariableInt(const char* objName, const char* varName, const int value);

#endif