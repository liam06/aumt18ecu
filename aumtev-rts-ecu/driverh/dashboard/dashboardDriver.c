#include "dashboardDriver.h"

#include <stdio.h>
#include <stdint.h>

#include "modules/operation/operationModule.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "driverl/gpio/gpio.h"
#include "driverl/adc/adc.h"
#include "lib/states.h"

// Define pins that lamps are connected to
const pin_t* pinVehicleReady = &P8_13;
const pin_t* pinIMD = &P8_15;
const pin_t* pinBMS = &P8_11;
const pin_t* pinBSPD = &P8_14;
const pin_t* pinPDOC = &P8_12;
const pin_t* pinECU = &P8_16;
// pins that buttons are connected to
const ain_t* pinDriverConfirm = &AIN3;    // BAIN7


// counter variables for "ready to drive" state
unsigned int vehicleReadyLEDTimer;
bool vehicleReadyLEDStrobe;


state_t dashboardDriverInit()
{
    printf("LOG: driverh/dashboard: Initializing dashboard\n");

    dashboardStatus = DASHBOARD_STATUS_INIT;
    vehicleReadyLEDStrobe = false;
    vehicleReadyLEDTimer = 0;

    return STATE_RUN;
}

state_t dashboardDriverUpdate()   // run at 10Hz
{
    // copy data from sysWarnings
    dashboardStatus.warningBMS = sysWarningStatus.errorBMS;
    dashboardStatus.warningIMD = sysWarningStatus.errorIMD;
    dashboardStatus.warningBSPD = sysWarningStatus.errorBSPD;
    dashboardStatus.warningPDOC = sysWarningStatus.errorPDOC;
    dashboardStatus.warningECU = sysWarningStatus.errorECU;


    // load driver confirm button
    int8_t driverConfirmInput = adcReadDigital(pinDriverConfirm);
    dashboardStatus.driverConfirmButton = (driverConfirmInput == 1) ? true : false;


    // update vehicle ready pin
    if (dashboardStatus.vehicleReadyStatus == VEHICLE_DRIVE_STATE_READY_TO_CHARGE || 
        dashboardStatus.vehicleReadyStatus == VEHICLE_DRIVE_STATE_CHARGING)
    {
        // strobe slowly
        if (++vehicleReadyLEDTimer == 10)
        {
            vehicleReadyLEDStrobe = !vehicleReadyLEDStrobe;
            vehicleReadyLEDTimer = 0;
        }
        gpioWrite(pinVehicleReady, vehicleReadyLEDStrobe);
    }
    else if (dashboardStatus.vehicleReadyStatus == VEHICLE_DRIVE_STATE_READY_TO_RUN)
    {
        // strobe faster; ready for driver input
        if (++vehicleReadyLEDTimer == 5)
        {
            vehicleReadyLEDStrobe = !vehicleReadyLEDStrobe;
            vehicleReadyLEDTimer = 0;
        }
        gpioWrite(pinVehicleReady, vehicleReadyLEDStrobe);
    }
    else if (dashboardStatus.vehicleReadyStatus == VEHILCE_DRIVE_STATE_RUNNING)
    {
        // running
        gpioWrite(pinVehicleReady, true);
    }
    else
    {
        gpioWrite(pinVehicleReady, false);
    }

    // write warnings to pins
    gpioWrite(pinIMD, dashboardStatus.warningIMD);
    gpioWrite(pinBMS, dashboardStatus.warningBMS);
    gpioWrite(pinBSPD, dashboardStatus.warningBSPD);
    gpioWrite(pinPDOC, dashboardStatus.warningPDOC);
    gpioWrite(pinECU, dashboardStatus.warningECU);

    return STATE_RUN;
}
