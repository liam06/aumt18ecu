#ifndef _DASHBOARDDRIVER_H
#define _DASHBOARDDRIVER_H

#include <stdint.h>
#include <stdbool.h>
#include "lib/states.h"

typedef struct {
    // inputs
    bool driverConfirmButton;

    // states
    uint8_t vehicleReadyStatus;
    bool warningIMD;
    bool warningBMS;
    bool warningBSPD;
    bool warningPDOC;
    bool warningECU;
} dashboard_t;
dashboard_t dashboardStatus;

static const dashboard_t DASHBOARD_STATUS_INIT =
{ 
    .driverConfirmButton = false, 
    .vehicleReadyStatus = 0,
    .warningIMD = false, 
    .warningBMS = false,
    .warningBSPD = false,
    .warningPDOC = false
};


extern state_t dashboardDriverInit();
extern state_t dashboardDriverUpdate();

#endif