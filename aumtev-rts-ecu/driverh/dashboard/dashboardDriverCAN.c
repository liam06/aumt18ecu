#include "dashboardDriver.h"

#include <stdio.h>

state_t dashboardDriverInit()
{
    printf("LOG: driverh/dashboard: Initializing dashboard (CAN)\n");

    dashboardStatus = DASHBOARD_STATUS_INIT;
    return STATE_RUN;
}

state_t dashboardDriverUpdate()   // run at 10Hz
{
    // Nothing to do
    return STATE_RUN;
}
