#ifndef _POWERSTAGESHUTDOWNDRIVER_H
#define _POWERSTAGESHUTDOWNDRIVER_H

#include <stdbool.h>
#include "lib/states.h"

extern state_t powerstageShutdownDriverInit();


// Controls whether tractive system is enabled/disabled
// enabled=true => the tractive system is powered
extern state_t powerstageShutdownDriverSetEnabled(bool enabled);

#endif