#include "powerstageShutdownDriver.h"

#include <stdio.h>
#include <stdbool.h>
#include "driverl/gpio/gpio.h"
#include "lib/states.h"

state_t powerstageShutdownDriverInit()
{
    printf("LOG: driverh/powerstageShutdown: Initializing powerstage shutdown driver\n");

    gpioInitPin(&P9_42, gpio_dir_out);
    
    printf("LOG: driverh/powerstageShutdown: Connecting high voltage!!!\n");
    powerstageShutdownDriverSetEnabled(true);

    return STATE_RUN;
}

state_t powerstageShutdownDriverSetEnabled(bool enabled)
{
    // TODO: update pin
    int ret = gpioWrite(&P9_42, enabled);
    return (ret == 0) ? STATE_RUN : STATE_ERROR;
}