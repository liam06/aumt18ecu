#include "appsBDriver.h"

#include <stdio.h>
#include <stdint.h>
#include "lib/states.h"
#include "driverl/adc/adc.h"
#include "appsBCalibration.h"

state_t appsBDriverState = STATE_INIT;


state_t appsBDriverInit()
{
    printf("LOG: driverh/appsB: Initializing throttle sensor\n");

    appsBSensorValue = 0.0f;

    appsBDriverSetState(STATE_RUN);

    return appsBDriverGetState();
}

state_t appsBDriverUpdate()
{
    appsBSensorValueRaw = adcRead(&AIN2);   // BAIN8 - APPS2

    // calculate the scaled sensor value
    // if (appsBSensorValueRaw < appsBPosition0)
    //     appsBSensorValue = 0.0f;
    // else if (appsBSensorValueRaw > appsBPosition1)
    //     appsBSensorValue = 1.0f;
    // else
    // {
        appsBSensorValue = ( (float)appsBSensorValueRaw - appsBPosition0 ) / (appsBPosition1-appsBPosition0);
    // }

    return appsBDriverGetState();
}

void appsBDriverSetState(const state_t state)
{
    appsBDriverState = state;
}
state_t appsBDriverGetState()
{
    return appsBDriverState;
}

