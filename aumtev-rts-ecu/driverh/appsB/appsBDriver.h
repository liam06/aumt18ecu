#ifndef _APPSBDRIVER_H
#define _APPSBDRIVER_H

#include <stdint.h>
#include "lib/states.h"

/*
 * appsBSensorValue: a value from 0 to 1 (0% to 100%) 
 * representing the current throttle value.
 */
float appsBSensorValue;
uint16_t appsBSensorValueRaw;


extern state_t appsBDriverInit();
extern state_t appsBDriverUpdate();

extern void appsBDriverSetState(const state_t state);
extern state_t appsBDriverGetState();

#endif