#ifndef _BPSADRIVER_H
#define _BPSADRIVER_H

#include <stdint.h>
#include "lib/states.h"

/*
 * bpsASensorValue: a value from 0 to 1 (0% to 100%) 
 * representing the current brake position value.
 */
float bpsASensorValue;
uint16_t bpsASensorValueRaw;


extern state_t bpsADriverInit();
extern state_t bpsADriverUpdate();

extern void bpsADriverSetState(const state_t state);
extern state_t bpsADriverGetState();

#endif