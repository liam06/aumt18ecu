#include "bpsADriver.h"
#include "bpsACabliration.h"

#include <stdio.h>
#include <stdint.h>
#include "lib/states.h"
#include "driverl/adc/adc.h"

state_t bpsADriverState = STATE_INIT;


state_t bpsADriverInit()
{
    printf("LOG: driverh/bpsA: Initializing throttle sensor\n");

    bpsASensorValue = 0.0f;
    bpsASensorValueRaw = 0;

    bpsADriverSetState(STATE_RUN);

    return bpsADriverGetState();
}

state_t bpsADriverUpdate()
{
    bpsASensorValueRaw = adcRead(&AIN1);   // BAIN3 - BPS1
    bpsASensorValue = ( (float)bpsASensorValueRaw - bpsAPosition0 ) / (bpsAPosition1-bpsAPosition0);

    return bpsADriverGetState();
}

void bpsADriverSetState(const state_t state)
{
    bpsADriverState = state;
}
state_t bpsADriverGetState()
{
    return bpsADriverState;
}

