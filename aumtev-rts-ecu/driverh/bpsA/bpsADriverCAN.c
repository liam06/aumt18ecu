#include "bpsADriver.h"
#include "bpsACabliration.h"

#include <stdio.h>
#include "lib/states.h"
#include "driverl/adc/adc.h"

state_t bpsADriverState = STATE_INIT;


state_t bpsADriverInit()
{
    printf("LOG: driverh/bpsA: Initializing throttle sensor\n");
    printf("LOG: driverh/bpsA: Operating via CAN bus\n");

    bpsASensorValue = 0.0f;

    bpsADriverSetState(STATE_RUN);
    return bpsADriverGetState();
}

state_t bpsADriverUpdate()
{
    bpsASensorValue = ( (float)bpsASensorValueRaw - bpsAPosition0 ) / (bpsAPosition1-bpsAPosition0);
    return bpsADriverGetState();
}

void bpsADriverSetState(const state_t state)
{
    bpsADriverState = state;
}
state_t bpsADriverGetState()
{
    return bpsADriverState;
}

