#ifndef _CANDRIVERINTERFACECOMMON_H
#define _CANDRIVERINTERFACECOMMON_H

#include <stdint.h>
#include <time.h>

// Last recieved times for CAN messages
#define NUM_CAN_IDS 0x7FF
struct timespec lastReceivedCanId[NUM_CAN_IDS];
extern void canDriverInterfaceTimeInit();

// CAN IDs for ECU nodes
#define CAN_ID_ECU_SENSORS1         0x200
#define CAN_ID_ECU_SENSORS2         0x201
#define CAN_ID_ECU_OPERATINGSTATE   0x210


// Demux messages from Inverter
extern void canDriverInterfaceDemuxInverter(uint16_t canId, uint8_t *data, uint8_t dataLen);
// Demux messages from BMS
extern void canDriverInterfaceDemuxBms(uint16_t canId, uint8_t *data, uint8_t dataLen);
// Demux messages from from ECU node
extern void canDriverInterfaceDemuxEcuFront(uint16_t canId, uint8_t *data, uint8_t dataLen);
// Demux messages from rear ECU node
extern void canDriverInterfaceDemuxEcuRear(uint16_t canId, uint8_t *data, uint8_t dataLen);

extern void canDriverInterfaceMuxEcuFront();
extern void canDriverInterfaceMuxEcuRear();

#endif