/*
 * Takes driver data communicated from CAN bus and 
 * places in the correct variable.
 */

#ifndef _CANDRIVERINTERFACE_H
#define _CANDRIVERINTERFACE_H

#include "lib/states.h"

extern state_t canDriverInterfaceInit();
extern state_t canDriverInterfaceUpdate();

extern void canDriverInterfaceSetState(const state_t state);
extern state_t canDriverInterfaceGetState();

#endif