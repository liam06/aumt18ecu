#include "canDriverInterface.h"

#include <stdio.h>

#include "lib/states.h"
#include "lib/bitsplit.h"
#include "driverl/can/can.h"
#include "canDriverInterfaceCommon.h"

state_t canDriverInterfaceState = STATE_INIT;

state_t canDriverInterfaceInit()
{
    printf("LOG: driverh/canDriverInterface: Initializing can driver interface\n");

    canDriverInterfaceTimeInit();

    canDriverInterfaceSetState(STATE_RUN);
    return canDriverInterfaceGetState();
}

state_t canDriverInterfaceUpdate()
{
    uint8_t dataLen;
    uint8_t data[8];
    uint16_t canId;

    // CAN Receiver
    while ((dataLen = canRead(data, &canId)) > 0)
    {
        // Data was received on CAN bus

        canDriverInterfaceDemuxInverter(canId, data, dataLen);
        canDriverInterfaceDemuxBms(canId, data, dataLen);
        canDriverInterfaceDemuxEcuRear(canId, data, dataLen);
    }

    // CAN Transmitter
    canDriverInterfaceMuxEcuFront();

    return canDriverInterfaceGetState();
}

void canDriverInterfaceSetState(const state_t state)
{
    canDriverInterfaceState = state;
}
state_t canDriverInterfaceGetState()
{
    return canDriverInterfaceState;
}

