#include "canDriverInterfaceCommon.h"

#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#include "lib/bitsplit.h"
#include "driverl/can/can.h"
#include "driverh/wheelSpeed/wheelSpeedFrontDriver.h"
#include "driverh/appsA/appsADriver.h"
#include "driverh/appsB/appsBDriver.h"
#include "driverh/bpps/bppsDriver.h"
#include "driverh/bpsA/bpsADriver.h"
#include "driverh/bpsB/bpsBDriver.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "driverh/dashboard/dashboardDriver.h"
#include "modules/operation/operationModule.h"

void canDriverInterfaceTimeInit()
{
    // init all timespec structs to 0
    int i;
    for (i = 0; i < NUM_CAN_IDS; i++)
    {
        lastReceivedCanId[i].tv_sec = 0;
        lastReceivedCanId[i].tv_nsec = 0;
    }
}

void canDriverInterfaceDemuxInverter(uint16_t canId, uint8_t *data, uint8_t dataLen)
{
    // update time
    clock_gettime(CLOCK_MONOTONIC, &lastReceivedCanId[canId]);

    switch (canId)
    {
        case CAN_ID_INVERTER_TEMPERATURES1:     // Inverter - Temperatures #1
            inverterSensorsRaw.temperatureModuleA = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.temperatureModuleB = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.temperatureModuleC = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.temperatureGateDriverBoard = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_TEMPERATURES2:     // Inverter - Temperatures #2
            inverterSensorsRaw.temperatureBoard = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.temperatureRTD1 = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.temperatureRTD2 = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.temperatureRTD3 = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_TEMPERATURES3:     // Inverter - Temperatures #3 & Torque Shudder
            inverterSensorsRaw.temperatureRTD4 = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.temperatureRTD5 = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.temperatureMotor = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.torqueShudder = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;

        case CAN_ID_INVERTER_ANALOGIN:     // Inverter - Analog Input Voltages
        {
            // TODO: check
            uint32_t inverterAinLow = combineBytesToInt(data+0, 4, endianLittle);
            uint32_t inverterAinHigh = combineBytesToInt(data+4, 4, endianLittle);

            inverterSensorsRaw.analogInput1 = (int16_t)((inverterAinLow >> 0) & 0x03FF); // 22 0s, 10 1s
            inverterSensorsRaw.analogInput2 = (int16_t)((inverterAinLow >> 10) & 0x03FF);
            inverterSensorsRaw.analogInput3 = (int16_t)((inverterAinLow >> 20) & 0x03FF);
            inverterSensorsRaw.analogInput4 = (int16_t)((inverterAinHigh >> 0) & 0x03FF);
            inverterSensorsRaw.analogInput5 = (int16_t)((inverterAinHigh >> 10) & 0x03FF);
            inverterSensorsRaw.analogInput6 = (int16_t)((inverterAinHigh >> 20) & 0x03FF);
            break;
        }

        case CAN_ID_INVERTER_DIGITALIN:     // Inverter - Digital Input Status:
            inverterSensorsRaw.digitalInput1 = (uint8_t)combineBytesToInt(data+0, 1, endianLittle);
            inverterSensorsRaw.digitalInput2 = (uint8_t)combineBytesToInt(data+1, 1, endianLittle);
            inverterSensorsRaw.digitalInput3 = (uint8_t)combineBytesToInt(data+2, 1, endianLittle);
            inverterSensorsRaw.digitalInput4 = (uint8_t)combineBytesToInt(data+3, 1, endianLittle);
            inverterSensorsRaw.digitalInput5 = (uint8_t)combineBytesToInt(data+4, 1, endianLittle);
            inverterSensorsRaw.digitalInput6 = (uint8_t)combineBytesToInt(data+5, 1, endianLittle);
            inverterSensorsRaw.digitalInput7 = (uint8_t)combineBytesToInt(data+6, 1, endianLittle);
            inverterSensorsRaw.digitalInput8 = (uint8_t)combineBytesToInt(data+7, 1, endianLittle);
            break;
        
        case CAN_ID_INVERTER_MOTORPOSITION:     // Inverter - Motor Position Information
            inverterSensorsRaw.motorAngle = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.motorSpeed = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.electricalFrequency = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.deltaResolver = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_CURRENT:     // Inverter - Current Information
            inverterSensorsRaw.currentPhaseA = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.currentPhaseB = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.currentPhaseC = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.currentDCBus = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_VOLTAGE:     // Inverter - Voltage Information
            inverterSensorsRaw.voltageDCBus = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.voltateOutput = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.voltageVAB_Vd = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.voltageVBC_Vq = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_FLUX:     // Inverter - Flux Information
            inverterSensorsRaw.fluxCommand = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.fluxFeedback = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.feedbackId = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.feedbackIq = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_INTERNALVOLT:     // Inverter - Internal Voltages
            inverterSensorsRaw.voltageReference1_5 = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.voltageReference2_5 = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.voltageReference5_0 = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.voltageReference12_0 = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_INTERNALSTATE:     // Inverter - Internal states
            inverterSensorsRaw.stateVSM = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.stateInverter = (uint8_t)combineBytesToInt(data+2, 1, endianLittle);
            inverterSensorsRaw.stateRelay = (uint8_t)combineBytesToInt(data+3, 1, endianLittle);
            inverterSensorsRaw.stateInverterRunMode = (uint8_t)combineBytesToInt(data+4, 1, endianLittle) & 0b00000001;
            inverterSensorsRaw.stateInverterActiveDischarge = (uint8_t)combineBytesToInt(data+4, 1, endianLittle) & 0b11100000;
            inverterSensorsRaw.stateInverterCommandMode = (uint8_t)combineBytesToInt(data+5, 1, endianLittle);
            inverterSensorsRaw.stateInverterEnable = (uint8_t)combineBytesToInt(data+6, 1, endianLittle) & 0b00000001;
            inverterSensorsRaw.stateInverterEnableLockout = (uint8_t)combineBytesToInt(data+6, 1, endianLittle) & 0b10000000;
            inverterSensorsRaw.directionCommand = (uint8_t)combineBytesToInt(data+7, 1, endianLittle);
            break;
        
        case CAN_ID_INVERTER_FAULTCODES:     // Inverter - Fault codes
            inverterSensorsRaw.faultPOSTLo = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.faultPOSTHi = (uint16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.faultRunLo = (uint16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.faultRunHi = (uint16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_TORQUETIMER:     // Inverter - torque & timer informaton
            inverterSensorsRaw.torqueCommanded = (int16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.torqueFeedback = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.timerPowerOn = (uint32_t)combineBytesToInt(data+4, 4, endianLittle);
            break;
        
        case CAN_ID_INVERTER_MODFLUXWEAK:     // Inverter - Moduleation index & flux weaking output information
            inverterSensorsRaw.modulationIndex = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.fluxWeakeningOutput = (int16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.commandId = (int16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.commandIq = (int16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_FIRMWARE:     // Inverter - Moduleation index & flux weaking output information
            inverterSensorsRaw.firmwareEEPROMVersion = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterSensorsRaw.firmwareSoftwareVersion = (uint16_t)combineBytesToInt(data+2, 2, endianLittle);
            inverterSensorsRaw.firmwareDateCodeMMDD = (uint16_t)combineBytesToInt(data+4, 2, endianLittle);
            inverterSensorsRaw.firmwareDateCodeYYYY = (uint16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_INVERTER_PARAM_READ:
            inverterParamOut.newMsg = true;
            inverterParamOut.paramAddress = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            inverterParamOut.writeSuccess = (bool)data[2];
            inverterParamOut.data = (uint16_t)combineBytesToInt(data+4, 2, endianLittle);
            break;
    }
}

void canDriverInterfaceDemuxBms(uint16_t canId, uint8_t *data, uint8_t dataLen)
{
    // update time
    clock_gettime(CLOCK_MONOTONIC, &lastReceivedCanId[canId]);

    switch (canId)
    {
        case CAN_ID_BMS_PACKVC:
            bmsSensorsRaw.packInstVoltage = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            bmsSensorsRaw.packOpenVoltage = (uint16_t)combineBytesToInt(data+2, 2, endianLittle);
            bmsSensorsRaw.packCurrent = (uint16_t)combineBytesToInt(data+4, 2, endianLittle);
            bmsSensorsRaw.averageCurrent = (uint16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;

        case CAN_ID_BMS_TEMPERATURES:
            bmsSensorsRaw.temperatureHigh = (uint8_t) data[0];
            bmsSensorsRaw.thermistorHigh = (uint8_t) data[1];
            bmsSensorsRaw.temperatureLow = (uint8_t) data[2];
            bmsSensorsRaw.thermistorLow = (uint8_t) data[3];
            bmsSensorsRaw.temperatureInternal = (uint8_t) data[4];
            break;
        
        case CAN_ID_BMS_CELLDATA:
            bmsSensorsRaw.opencellHighVoltage = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            bmsSensorsRaw.opencellHighID = (uint8_t) data[2];
            bmsSensorsRaw.opencellLowVoltage = (uint16_t)combineBytesToInt(data+3, 2, endianLittle);
            bmsSensorsRaw.opencellLowID = (uint8_t) data[5];
            bmsSensorsRaw.opencellAvgVoltage = (uint16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_BMS_CELLRESIST:
            bmsSensorsRaw.resistanceCellHigh = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            bmsSensorsRaw.resistanceCellHighID = (uint8_t) data[2];
            bmsSensorsRaw.resistanceCellLow = (uint16_t)combineBytesToInt(data+3, 2, endianLittle);
            bmsSensorsRaw.resistanceCellLowID = (uint8_t) data[5];
            bmsSensorsRaw.resistanceCellAvg = (uint16_t)combineBytesToInt(data+6, 2, endianLittle);
            break;
        
        case CAN_ID_BMS_MISC:
            bmsSensorsRaw.failsafeStatuses = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            bmsSensorsRaw.rollingCounter = (uint8_t) data[2];
            bmsSensorsRaw.packSOC = (uint8_t) data[3];
            bmsSensorsRaw.adaptiveSOC = (uint8_t) data[4];
            bmsSensorsRaw.packDCLMax = (uint16_t)combineBytesToInt(data+5, 2, endianLittle);
            break;
    }
}

void canDriverInterfaceDemuxEcuFront(uint16_t canId, uint8_t *data, uint8_t dataLen)
{
    // update time
    clock_gettime(CLOCK_MONOTONIC, &lastReceivedCanId[canId]);

    switch (canId)
    {
        case CAN_ID_ECU_SENSORS1:     // Sensors #1
            // APPS A in data[0:1] (i.e. data[0] and data[1])
            // BPPS in data[2:3]
            // BPS A in data[4:5]
            // Wheel speed FR in data[6:7]

            appsASensorValueRaw = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            bppsSensorValueRaw = (uint16_t)combineBytesToInt(data+2, 2, endianLittle);
            bpsASensorValueRaw = (uint16_t)combineBytesToInt(data+4, 2, endianLittle);
            wheelSpeedFR.revTimeRaw = (uint16_t)combineBytesToInt(data+6, 2, endianLittle);

            break;
        
        case CAN_ID_ECU_SENSORS2:     // Sensors #2
            // APPS B in data[0:1] (i.e. data[0] and data[1])
            // BPS B in data[2:3]
            // BPS A in data[4:5]
            // Wheel speed FR in data[6:7]

            appsBSensorValueRaw = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
            bpsBSensorValueRaw = (uint16_t)combineBytesToInt(data+2, 2, endianLittle);
            wheelSpeedFL.revTimeRaw = (uint16_t)combineBytesToInt(data+4, 2, endianLittle);
            dashboardStatus.driverConfirmButton = (bool)data[6];

            break;
    }
}

void canDriverInterfaceDemuxEcuRear(uint16_t canId, uint8_t *data, uint8_t dataLen)
{
    // update time
    clock_gettime(CLOCK_MONOTONIC, &lastReceivedCanId[canId]);

    switch (canId)
    {
        case CAN_ID_ECU_OPERATINGSTATE:     // Sensors #1
            // IMD error in data[0]
            // BMS error in data[1]
            // BSPD error in data[2]
            // PDOC error in data[3]
            // Drive state in data[4]
            sysWarningStatus.errorIMD = (bool)data[0];
            sysWarningStatus.errorBMS = (bool)data[1];
            sysWarningStatus.errorBSPD = (bool)data[2];
            sysWarningStatus.errorPDOC = (bool)data[3];
            sysWarningStatus.errorECU = (bool)data[4];
            dashboardStatus.vehicleReadyStatus = data[5];

            break;
    }
}

void canDriverInterfaceMuxEcuFront()
{
    uint8_t dataLen;
    uint8_t data[8];
    uint16_t canId;


    // Sensors #1
    canId = CAN_ID_ECU_SENSORS1;
    dataLen = 8;
    splitIntToBytes(appsASensorValueRaw, data+0, 2, endianLittle);
    splitIntToBytes(bppsSensorValueRaw, data+2, 2, endianLittle);
    splitIntToBytes(bpsASensorValueRaw, data+4, 2, endianLittle);
    splitIntToBytes(wheelSpeedFR.revTimeRaw, data+6, 2, endianLittle);
    canWrite(canId, dataLen, data);

    // Sensors #2
    canId = CAN_ID_ECU_SENSORS2;
    dataLen = 8;
    splitIntToBytes(appsBSensorValueRaw, data+0, 2, endianLittle);
    splitIntToBytes(bpsBSensorValueRaw, data+2, 2, endianLittle);
    splitIntToBytes(wheelSpeedFL.revTimeRaw, data+4, 2, endianLittle);
    data[6] = (uint8_t)dashboardStatus.driverConfirmButton;
    data[7] = (uint8_t)0x00;
    canWrite(canId, dataLen, data);
}

void canDriverInterfaceMuxEcuRear()
{
    uint8_t dataLen;
    uint8_t data[8];
    uint16_t canId;


    // Operating state
    canId = CAN_ID_ECU_OPERATINGSTATE;
    dataLen = 8;
    data[0] = sysWarningStatus.errorIMD;
    data[1] = sysWarningStatus.errorBMS;
    data[2] = sysWarningStatus.errorBSPD;
    data[3] = sysWarningStatus.errorPDOC;
    data[4] = sysWarningStatus.errorECU;
    data[5] = vehicleDriveState;
    data[6] = 0x00;
    data[7] = 0x00; 
    canWrite(canId, dataLen, data);

}