/*
 * Shows the value of the front right and left wheel speeds
 */

#ifndef _WHEELSPEEDFRONTDRIVER_H
#define _WHEELSPEEDFRONTDRIVER_H

#include "wheelSpeedDecoder.h"

#include <stdint.h>
#include "lib/states.h"
/*
 * wheelSpeedFL: speed of front left wheel
 * wheelSpeedRT: speed of front right wheel
 */
wheelSpeed_t wheelSpeedFL;
wheelSpeed_t wheelSpeedFR;

extern state_t wheelSpeedFrontDriverInit();
extern state_t wheelSpeedFrontDriverUpdate();

extern void wheelSpeedFrontDriverSetState(const state_t state);
extern state_t wheelSpeedFrontDriverGetState();

#endif