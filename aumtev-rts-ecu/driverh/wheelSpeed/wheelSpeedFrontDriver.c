#include "wheelSpeedFrontDriver.h"
#include "wheelSpeedDecoder.h"

#include <stdio.h>
#include <stdint.h>

#include "lib/states.h"
#include "driverl/adc/adc.h"

state_t wheelSpeedFrontDriverState = STATE_INIT;

// Wheel speed decoder data
wheelSpeedDecoder_t wheelSpeedDecoderFL;
wheelSpeedDecoder_t wheelSpeedDecoderFR;

state_t wheelSpeedFrontDriverInit()
{
    printf("LOG: driverh/wheelSpeedFront: Initializing throttle sensor\n");

    // Initialize decoder
    wheelSpeedDecoderInit(&wheelSpeedFL, &wheelSpeedDecoderFL); // left wheel
    wheelSpeedDecoderInit(&wheelSpeedFR, &wheelSpeedDecoderFR); // right wheel

    wheelSpeedFrontDriverSetState(STATE_RUN);
    return wheelSpeedFrontDriverGetState();
}

state_t wheelSpeedFrontDriverUpdate()
{
    uint16_t adcVal_FR = adcRead(&AIN4);   // BAIN2 - FR
    uint16_t adcVal_FL = adcRead(&AIN5);   // BAIN1 - FL

    // // Update raw data from input
    // wheelSpeedDecoderUpdate(adcVal_FL, &wheelSpeedFL, &wheelSpeedDecoderFL);
    // wheelSpeedDecoderUpdate(adcVal_FR, &wheelSpeedFR, &wheelSpeedDecoderFR);


    // // Update calculated fields
    // wheelSpeedDecoderUpdateCalculations(&wheelSpeedFL);
    // wheelSpeedDecoderUpdateCalculations(&wheelSpeedFR);

    return wheelSpeedFrontDriverGetState();
}

void wheelSpeedFrontDriverSetState(const state_t state)
{
    wheelSpeedFrontDriverState = state;
}
state_t wheelSpeedFrontDriverGetState()
{
    return wheelSpeedFrontDriverState;
}

