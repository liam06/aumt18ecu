#ifndef _WHEELSPEEDCONSTANTS_H
#define _WHEELSPEEDCONSTANTS_H

#include <stdint.h>

static const uint16_t WHEEL_NUM_TEETH = 12;
static const float WHEEL_DIAMETER = 0.52f; // metres

static const uint16_t WHEEL_ADC_THRESHOLD_HIGH = 65000;
static const uint16_t WHEEL_ADC_THRESHOLD_LOW = 5000;

#endif