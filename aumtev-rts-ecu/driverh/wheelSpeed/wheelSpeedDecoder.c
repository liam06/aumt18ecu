#include "wheelSpeedDecoder.h"
#include "wheelSpeedConstants.h"

#include <time.h>
#include <math.h>

// Initializes the fields for a wheel speed decoder in a wheel speed driver
void wheelSpeedDecoderInit(wheelSpeed_t *wheelSpeedData, 
                           wheelSpeedDecoder_t *decoderData)
{
    // Default data values
    *wheelSpeedData = WHEEL_SPEED_INIT;

    // Initialize state machine and decoder
    decoderData->state = wheelSpeedDecoderS0;
    decoderData->counterCodeTicks = 0;
    decoderData->counterSpikes = 0;
    clock_gettime(CLOCK_MONOTONIC, decoderData->timeStart); // start of timer
}

// MUST BE CALLED EVERY 1ms
// Updates the state machine and raw output data for a wheel speed
// updates the fields wheelSpeed_t.revTimeRaw and wheelSpeed_t.revTime
void wheelSpeedDecoderUpdate(const uint16_t adcInput,
                             wheelSpeed_t *wheelSpeedData,
                             wheelSpeedDecoder_t *decoderData)
{

    // convert 0/1 signal into 0->1 transitions
    if (decoderData->state == wheelSpeedDecoderS0)
    {
        if (adcInput > WHEEL_ADC_THRESHOLD_HIGH) // ADC HIGH input threshold
        {
            //transition 0->1 occured
            decoderData->state = wheelSpeedDecoderS1; // move to S1
        } // otherwise stay as is
    }
    else if (decoderData->state == wheelSpeedDecoderS1)
    {
        // count 0->1 transition
        decoderData->counterSpikes = decoderData->counterSpikes + 1;
        decoderData->state = wheelSpeedDecoderS2;
    }
    else if (decoderData->state == wheelSpeedDecoderS2)
    {
        if (adcInput < WHEEL_ADC_THRESHOLD_LOW) // ADC LOW input threshold
        {
            //transition 1->0 occured
            decoderData->state = wheelSpeedDecoderS0;
        } // otherwise stay as is
    }

    // have we counted 10 samples?
    if (++decoderData->counterCodeTicks >= 10)
    {
        // have counted 10 samples, how long did that take?
        struct timespec timeNow;
        double dt; // time taken for the 12 0->1 transitions - i.e. time for 1 rev.
        clock_gettime(CLOCK_MONOTONIC, &timeNow); // get time now
        dt = ((double)timeNow.tv_sec + 1.0e-9*timeNow.tv_nsec) - ((double)decoderData->timeStart->tv_sec + 1.0e-9*decoderData->timeStart->tv_nsec);

        // haven't necessarily done one full wheel rotation
        float wheelFractionPassed = (float)decoderData->counterSpikes / WHEEL_NUM_TEETH;

        // update raw value
        wheelSpeedData->revTimeRaw = (uint16_t)(wheelFractionPassed * dt * 1e5);

        // reset counter
        decoderData->counterSpikes = 0;
        decoderData->counterCodeTicks = 0;
        clock_gettime(CLOCK_MONOTONIC, decoderData->timeStart); // start timing again
    }
}

// Calculates the the following fields within wheelSpeed_t:
//      wheelSpeedRPM
//      groundSpeedMps
//      groundSpeedKph
void wheelSpeedDecoderUpdateCalculations(wheelSpeed_t *wheelSpeedData)
{
    wheelSpeedData->revTime = (double)wheelSpeedData->revTimeRaw / 1.0e5;
    wheelSpeedData->wheelSpeedRPM = 60.0f / wheelSpeedData->revTime;
    wheelSpeedData->groundSpeedMps = M_PI * WHEEL_DIAMETER * (float)wheelSpeedData->wheelSpeedRPM / 60.0f;
    wheelSpeedData->groundSpeedKph = 3.6f * wheelSpeedData->groundSpeedMps;
}
