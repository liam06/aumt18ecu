#include "wheelSpeedFrontDriver.h"
#include "wheelSpeedDecoder.h"

#include <stdio.h>

#include "lib/states.h"

state_t wheelSpeedFrontDriverState = STATE_INIT;


state_t wheelSpeedFrontDriverInit()
{
    printf("LOG: driverh/wheelSpeedFront: Initializing wheel speed (front) sensors\n");
    printf("LOG: driverh/wheelSpeedFront: Operating via CAN bus\n");

    wheelSpeedFL = WHEEL_SPEED_INIT;
    wheelSpeedFR = WHEEL_SPEED_INIT;

    wheelSpeedFrontDriverSetState(STATE_RUN);
    return wheelSpeedFrontDriverGetState();
}

state_t wheelSpeedFrontDriverUpdate()
{
    // Update calculated fields
    wheelSpeedDecoderUpdateCalculations(&wheelSpeedFL);
    wheelSpeedDecoderUpdateCalculations(&wheelSpeedFR);
    return wheelSpeedFrontDriverGetState();
}

void wheelSpeedFrontDriverSetState(const state_t state)
{
    wheelSpeedFrontDriverState = state;
}
state_t wheelSpeedFrontDriverGetState()
{
    return wheelSpeedFrontDriverState;
}

