/*
 * Shows the value of the front right and left wheel speeds
 */

#ifndef _WHEELSPEEDREARDRIVER_H
#define _WHEELSPEEDREARDRIVER_H

#include "wheelSpeedDecoder.h"

#include <stdint.h>
#include "lib/states.h"
/*
 * wheelSpeedRL: speed of rear left wheel
 * wheelSpeedRR: speed of rear right wheel
 */
wheelSpeed_t wheelSpeedRL;
wheelSpeed_t wheelSpeedRR;

extern state_t wheelSpeedRearDriverInit();
extern state_t wheelSpeedRearDriverUpdate();

extern void wheelSpeedRearDriverSetState(const state_t state);
extern state_t wheelSpeedRearDriverGetState();

#endif