#ifndef _WHEELSPEEDDECODER_H
#define _WHEELSPEEDDECODER_H

// Defines the state machine and logic for running a wheel speed driver
// Note: This is not a driver

#include <stdint.h>
#include <time.h>

// struct to contain data for wheel speeds
typedef struct {
    uint16_t revTimeRaw; // time for 1 revolution
    double revTime; // time for 1 revolution
    float wheelSpeedRPM;
    float groundSpeedMps; // ground speed in m/s
    float groundSpeedKph; // ground speed in km/h
} wheelSpeed_t;

static const wheelSpeed_t WHEEL_SPEED_INIT = 
{ .revTimeRaw=0, .revTime=0.0, .wheelSpeedRPM=0.0f, .groundSpeedMps=0.0f, .groundSpeedKph=0.0f};

typedef enum {
    wheelSpeedDecoderS0 = 0,
    wheelSpeedDecoderS1 = 1,
    wheelSpeedDecoderS2 = 2,
} wheelSpeedDecoderState_t;

// struct to store data for wheel speed decoder
typedef struct {
    struct timespec *timeStart;    // when decoder started monitoring current cycle
    wheelSpeedDecoderState_t state; // decoder state
    uint8_t counterSpikes;   // count of 0->1 transitions
    uint8_t counterCodeTicks; // count of how many times decoder has been called
} wheelSpeedDecoder_t;


// Initializes the fields for a wheel speed decoder in a wheel speed driver
extern void wheelSpeedDecoderInit(wheelSpeed_t *wheelSpeedData,
                                  wheelSpeedDecoder_t *decoderData);

// Updates the state machine and raw output data for a wheel speed
// updates the fields wheelSpeed_t.revTimeRaw and wheelSpeed_t.revTime
extern void wheelSpeedDecoderUpdate(const uint16_t adcInput,
                                    wheelSpeed_t *wheelSpeedData,
                                    wheelSpeedDecoder_t *decoderData);

// Calculates the the following fields within wheelSpeed_t:
//      wheelSpeedRPM
//      groundSpeedMps
//      groundSpeedKph
extern void wheelSpeedDecoderUpdateCalculations(wheelSpeed_t *wheelSpeedData);

#endif