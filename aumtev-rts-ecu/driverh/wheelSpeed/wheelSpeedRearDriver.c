#include "wheelSpeedRearDriver.h"
#include "wheelSpeedDecoder.h"

#include <stdio.h>
#include <stdint.h>

#include "lib/states.h"
#include "driverl/adc/adc.h"

state_t wheelSpeedRearDriverState = STATE_INIT;

// Wheel speed decoder data
wheelSpeedDecoder_t wheelSpeedDecoderRL;
wheelSpeedDecoder_t wheelSpeedDecoderRR;


state_t wheelSpeedRearDriverInit()
{
    printf("LOG: driverh/wheelSpeedRear: Initializing throttle sensor\n");

    // Initialize decoder
    wheelSpeedDecoderInit(&wheelSpeedRL, &wheelSpeedDecoderRL); // left wheel
    wheelSpeedDecoderInit(&wheelSpeedRR, &wheelSpeedDecoderRR); // right wheel

    wheelSpeedRearDriverSetState(STATE_RUN);
    return wheelSpeedRearDriverGetState();
}

state_t wheelSpeedRearDriverUpdate()
{
    uint16_t adcVal_RR = adcRead(&AIN7);   // BAIN5 - RR
    uint16_t adcVal_RL = adcRead(&AIN6);   // BAIN6 - RL

    // // Update raw data from input
    // wheelSpeedDecoderUpdate(adcVal_RL, &wheelSpeedRL, &wheelSpeedDecoderRL);
    // wheelSpeedDecoderUpdate(adcVal_RR, &wheelSpeedRR, &wheelSpeedDecoderRR);

    // // Update calculated fields
    // wheelSpeedDecoderUpdateCalculations(&wheelSpeedRL);
    // wheelSpeedDecoderUpdateCalculations(&wheelSpeedRR);

    return wheelSpeedRearDriverGetState();
}

void wheelSpeedRearDriverSetState(const state_t state)
{
    wheelSpeedRearDriverState = state;
}
state_t wheelSpeedRearDriverGetState()
{
    return wheelSpeedRearDriverState;
}

