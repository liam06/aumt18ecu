#include "system/sysInit.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "system/rtTickManager.h"

#include "driverl/adc/adc.h"
#include "driverl/can/can.h"
#include "driverl/spi/spi.h"
#include "driverl/gpio/gpio.h"

#include "driverh/canDriverInterface/canDriverInterfaceCommon.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverh/wheelSpeed/wheelSpeedFrontDriver.h"
#include "driverh/appsA/appsADriver.h"
#include "driverh/appsB/appsBDriver.h"
#include "driverh/bpps/bppsDriver.h"
#include "driverh/bpsA/bpsADriver.h"
#include "driverh/bpsB/bpsBDriver.h"
#include "driverh/powerstageShutdown/powerstageShutdownDriver.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "driverh/dashboard/dashboardDriver.h"

#include "lib/bitsplit.h"


int counter = 0;
state_t monitorInit();
state_t monitorTick();

int main(int argc, char *argv[])
{
	printf("LOG: main: Starting system (monitor)\n");

	// Create reference of all modules & drivers for initialization and RT calling
	rtCall calls[] = {
		//format: {init function, tick function, tick divider}
		// the tick functions execute in this order

		// Drivers (low level) ----------------------------
		{&gpioInit, NULL, 0, 0},
		{&spiInit, NULL, 0, 0},
		{&canInit, NULL, 0, 0},
		{&adcInit, &adcUpdate, 1, 0},

		// Drivers (high level) ---------------------------
		{&inverterDriverInit, &inverterDriverUpdate, 1, 0},
		{&bmsDriverInit, &bmsDriverUpdate, 1, 0},
		{&wheelSpeedFrontDriverInit, &wheelSpeedFrontDriverUpdate, 1, 0},
		{&appsADriverInit, &appsADriverUpdate, 1, 0},
		{&appsBDriverInit, &appsBDriverUpdate, 1, 0},
		{&bppsDriverInit, &bppsDriverUpdate, 1, 0},
		{&bpsADriverInit, &bpsADriverUpdate, 1, 0},
		{&bpsBDriverInit, &bpsBDriverUpdate, 1, 0},
		{&sysWarningsInit, &sysWarningsUpdate, 1, 0},

		// Monitor output
		{&monitorInit, &monitorTick, 100, 0},
	};

	// Init components
	printf("LOG: main: Initializing components\n");
	counter = 0;
	bool initSuccess = sysInit(calls, sizeof(calls) / sizeof(calls[0]));

	if (!initSuccess)
	{
		printf("ERROR: main: Initialization failed\n");
		return -1;
	}

	// Init system components
	printf("LOG: main: Initializing rtTickManager\n");
	rtTickManagerInit(100, 0);
	printf("LOG: main: Beginning RT operation\n");
	rtTickManagerRun(calls, sizeof(calls) / sizeof(calls[0]));
	

	return 1;   // Shouldn't have reached here
}


state_t monitorInit()
{
	return STATE_RUN;
}

state_t monitorTick()
{
	uint8_t dataLen;
    uint8_t data[8];
    uint16_t canId = 0;
	
	uint16_t raw;
	uint16_t msgCount = 0;
	float torque = 0.0f;

    // CAN Receiver
    while ((dataLen = canRead(data, &canId)) > 0)
    {
        // Data was received on CAN bus
		msgCount++;
        canDriverInterfaceDemuxInverter(canId, data, dataLen);
        canDriverInterfaceDemuxBms(canId, data, dataLen);
        canDriverInterfaceDemuxEcuFront(canId, data, dataLen);

		// other cases
		switch (canId)
		{
			case 0x0C0:
				raw = (uint16_t)combineBytesToInt(data+0, 2, endianLittle);
				torque = 10.0f * (float)raw;
				break;
		}
    }

	printf("Monitor output #%u\n", ++counter);
	printf("Message received since last: %u\n", msgCount);
	printf("Last message recieved with ID %x\n", canId);

	printf("ECU messages\n");
	printf("\tAPPS A:                %f\n", appsASensorValue);
	printf("\tAPPS A (raw):          %d\n", appsASensorValueRaw);
	printf("\tAPPS B:                %f\n", appsBSensorValue);
	printf("\tAPPS B (raw):          %d\n", appsBSensorValueRaw);
	printf("\tBPS:                   %f\n", bpsASensorValue);
	printf("\tBPS (raw):             %d\n", bpsASensorValueRaw);
	printf("\tRTD button:            %d\n", dashboardStatus.driverConfirmButton);
	printf("\tTorque:                %f\n", torque);
	// printf("\tError IMD:             %d\n", sysWarningStatus.errorIMD);
	// printf("\tError BMS:             %d\n", sysWarningStatus.errorBMS);
	// printf("\tError BSPD:            %d\n", sysWarningStatus.errorBSPD);
	// printf("\tError PDOC:            %d\n", sysWarningStatus.errorPDOC);
	// printf("\tError ECU:             %d\n", sysWarningStatus.errorECU);

	printf("Inverter messages\n");
	printf("\tModuleA temp:          %f\n", inverterSensors.temperatureModuleA);
    printf("\tMobuleB temp:          %f\n", inverterSensors.temperatureModuleB);
    printf("\tModuleC temp:          %f\n", inverterSensors.temperatureModuleC);
    printf("\tGDB temp:              %f\n", inverterSensors.temperatureGateDriverBoard);
    printf("\tBoard temp:            %f\n", inverterSensors.temperatureBoard);
    printf("\tMotor temp:            %f\n", inverterSensors.temperatureMotor);
	printf("\tTorque fdbk:           %f\n", inverterSensors.torqueFeedback);
	// printf("\tMotor angle:           %f\n", inverterSensors.motorAngle);
	printf("\tMotor speed:           %d\n", inverterSensors.motorSpeed);
	printf("\tI (DC):                %f\n", inverterSensors.currentDCBus);
	// printf("\tI (Phase A):           %f\n", inverterSensors.currentPhaseA);
	// printf("\tI (Phase B):           %f\n", inverterSensors.currentPhaseB);
	// printf("\tI (Phase C):           %f\n", inverterSensors.currentPhaseC);
	printf("\tV (DC):                %f\n", inverterSensors.voltageDCBus);
	// printf("\tV (VAB_Vd):            %f\n", inverterSensors.voltageVAB_Vd);
	// printf("\tV (VBC_Vq):            %f\n", inverterSensors.voltageVBC_Vq);
	printf("\tV (output):            %f\n", inverterSensors.voltateOutput);
	printf("\tVSM state:             %d\n", inverterSensors.stateVSM);
	printf("\tFault POST Lo:         %x\n", inverterSensors.faultPOSTLo);
	printf("\tFault POST Hi:         %x\n", inverterSensors.faultPOSTHi);
	printf("\tFault RUN Lo:          %x\n", inverterSensors.faultRunLo);
	printf("\tFault RUN Hi:          %x\n", inverterSensors.faultRunHi);

	printf("BMS messages\n");
	printf("\tPack inst volatge:     %f\n", bmsSensors.packInstVoltage);
	printf("\tPack open volatge:     %f\n", bmsSensors.packOpenVoltage);
	printf("\tPack current:          %f\n", bmsSensors.packCurrent);
	printf("\tPower (calulated):     %f\n", bmsSensors.packInstPower);
	printf("\tAverage current:       %f\n", bmsSensors.averageCurrent);
	printf("\tTemperature (high):    %f\n", bmsSensors.temperatureHigh);
	// printf("\tThermistor (high):     %f\n", bmsSensors.thermistorHigh);
	printf("\tTemperature (low):     %f\n", bmsSensors.temperatureLow);
	// printf("\tThermistor (low):      %f\n", bmsSensors.thermistorLow);
	printf("\tTemperature interanl:  %f\n", bmsSensors.temperatureInternal);
	printf("\tPack SOC:  %f\n", bmsSensors.packSOC);

	printf("\n");
	return STATE_RUN;
}