#include "system/sysInit.h"

#include <stdio.h>
#include <stdint.h>


#include "system/rtTickManager.h"

#include "driverl/adc/adc.h"
#include "driverl/can/can.h"
#include "driverl/spi/spi.h"
#include "driverl/gpio/gpio.h"
#include "driverl/uart/uart.h"

#include "driverh/canDriverInterface/canDriverInterface.h"
#include "driverh/canDriverInterface/canDriverInterfaceCommon.h"
#include "driverh/inverter/inverterDriver.h"
#include "driverh/bms/bmsDriver.h"
#include "driverh/screen/screenDriver.h"
#include "driverh/wheelSpeed/wheelSpeedFrontDriver.h"
#include "driverh/appsA/appsADriver.h"
#include "driverh/appsB/appsBDriver.h"
#include "driverh/bpps/bppsDriver.h"
#include "driverh/bpsA/bpsADriver.h"
#include "driverh/bpsB/bpsBDriver.h"
#include "driverh/sysWarnings/sysWarnings.h"
#include "driverh/dashboard/dashboardDriver.h"


int run(const unsigned int numTicks);


int main(int argc, char *argv[])
{
	run(0);
	

	return 1;   // Shouldn't have reached here
}

int run(const unsigned int numTicks)
{
	printf("LOG: main: Starting system (front)\n");

	// Create reference of all modules & drivers for initialization and RT calling
	rtCall calls[] = {
		//format: {init function, tick function, tick divider}
		// the tick functions execute in this order

		// Drivers (low level) ----------------------------
		{&gpioInit, NULL, 0, 0},
		{&spiInit, NULL, 0, 0},
		{&canInit, &canUpdate, 1, 0},
		// {&uartInit, NULL, 0, 0},
		{&adcInit, &adcUpdate, 1, 0},

		// Drivers (high level) ---------------------------
		{&inverterDriverInit, &inverterDriverUpdate, 1, 0},
		{&bmsDriverInit, &bmsDriverUpdate, 1, 0},
		{&sysWarningsInit, &sysWarningsUpdate, 1, 0},
		// {&screenDriverInit, &screenDriverUpdate, 2, 0},
		{&wheelSpeedFrontDriverInit, &wheelSpeedFrontDriverUpdate, 1, 0},
		{&appsADriverInit, &appsADriverUpdate, 1, 0},
		{&appsBDriverInit, &appsBDriverUpdate, 1, 0},
		{&bppsDriverInit, &bppsDriverUpdate, 1, 0},
		{&bpsADriverInit, &bpsADriverUpdate, 1, 0},
		{&bpsBDriverInit, &bpsBDriverUpdate, 1, 0},
		{&dashboardDriverInit, &dashboardDriverUpdate, 1, 0},
		{&canDriverInterfaceInit, &canDriverInterfaceUpdate, 1, 0},
	};

	// Init components
	printf("LOG: main: Initializing components\n");
	bool initSuccess = sysInit(calls, sizeof(calls) / sizeof(calls[0]));

	if (!initSuccess)
	{
		printf("ERROR: main: Initialization failed\n");
		return -1;
	}


	// make sure APPS sends out zero
	appsASensorValueRaw = 0;
	appsBSensorValueRaw = 0;
	canDriverInterfaceMuxEcuFront();


	// Init system components
	rtTickManagerInit(100, numTicks);
	rtTickManagerRun(calls, sizeof(calls) / sizeof(calls[0]));

	return 1;
}
