#include "system/sysInit.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "system/rtTickManager.h"

#include "driverl/adc/adc.h"
#include "driverl/can/can.h"
#include "driverl/spi/spi.h"
#include "driverh/canDriverInterface/canDriverInterface.h"
#include "driverh/appsA/appsADriver.h"
#include "modules/control/controlModule.h"
#include "modules/operation/operationModule.h"


int main(int argc, char *argv[])
{
	int resultInit;

	printf("CAN TEST ----------------------------\n");
	resultInit = canInit();
	printf("CAN init result: %d\n", resultInit);

	uint8_t data1[] = {0x44, 0x4A, 0x03};
	uint8_t data2[] = {0x44, 0x4B, 0xBB};
	uint8_t data3[] = {0xDE, 0xFF, 0x00};
	uint8_t data4[] = {0x00, 0x4F, 0xFF, 0xFF, 0xFF, 0x34, 0x00, 0xFF};

	canWrite(0x1F1, 3, data1);
	canWrite(0x1F2, 3, data2);
	canWrite(0x1F3, 3, data3);
	canWrite(0x1F4, 8, data4);
	printf("DONE --------------------------------\n\n");

	uint8_t dataLen;
    uint8_t data[8];
    uint16_t canId;

    // CAN Receiver
	while (true) {
		while ((dataLen = canRead(data, &canId)) > 0){
			printf("Read ID: %x\n", canId);
		}

		usleep(100000);

		dataLen = 4;
		data[0] = 0x11;
		data[1] = 0x22;
		data[2] = 0x33;
		data[3] = 0x44;
		canId = 0x333;
		canWrite(canId, dataLen, data);
	}

	// printf("ADC TEST ----------------------------\n");
	// resultInit = adcInit();
	// printf("ADC init result: %d\n", resultInit);

	// adcUpdate();
	// int adcResult = adcRead(&AIN0);

    // printf("ADC output test: %d\n", adcResult);
	// printf("DONE --------------------------------\n\n");
	

	return 0;
}