#include "sysInit.h"

#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include "lib/states.h"
#include "system/rtTickManager.h"
#include "driverl/can/can.h"

bool sysInit(rtCall *calls, const unsigned int numComponents)
{
	printf("LOG: sysInit: Initializing software components\n");

	unsigned int i;
	for (i = 0; i < numComponents; ++i)
	{
		state_t result = calls[i].initFunction();

		if (result == STATE_ERROR)
		{
			return false;
		}

		// set initial rtCall values (TODO: do this more elegantly)
		calls[i].tickDividerCount = 0;
	}	

	return true;
}

bool sysInitNodeAck(uint8_t masterId, uint8_t targetId, uint16_t timeout_ms)
{
	if (canState != STATE_RUN)
	{
		return false;
	}

	// storage for timeouts
	bool timeout = true;
	struct timespec tstart = {0,0};
	struct timespec tend = {0,0};

	// we need to send a request & receieve the response for that (response)
	// and need to get a request from the other node (request)
	bool request = false;
	bool response = false;

	while (request == false && response == false)
	{
		uint8_t dataLen;
		uint8_t data[8];
		uint16_t canId;
		
		// check whether timeout occured
		if (timeout == false)
		{
			// calculate time diff
			clock_gettime(CLOCK_MONOTONIC, &tend);
			double diff_s = ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - 
    						((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec);
			
			if (diff_s >= timeout_ms*1000)
			{
				printf("LOG: sysInit: sysInitNodeAck timeout\n");
				timeout = true;
			}
		}

		// check if we need to send a new request
		if (timeout && response == false)
		{
			// send new request
			dataLen = 2;
			data[0] = masterId;
			data[1] = targetId;
			canId = 0x4C0;  // CAN ID for an ACK request
			canWrite(canId, dataLen, data);

			// calculate time
			timeout = false;
			clock_gettime(CLOCK_MONOTONIC, &tstart);
		}

		// now try to read the response
		while ((dataLen = canRead(data, &canId)) > 0)
		{
			if (canId == 0x4C0 && dataLen == 2)	  // CAN ID for ACK request
			{
				if (data[0] == targetId && data[1] == masterId)
				{
					response = true;

					// send response
					dataLen = 2;
					data[0] = masterId;
					data[1] = targetId;
					canId = 0x4C1;
					canWrite(canId, dataLen, data);
				}
			}
			else if (canId == 0x4C1 && dataLen == 2)  // CAN ID for ACK response
			{
				if (data[0] == targetId &&  // sender of the ACK should be the ACK target
					data[1] == masterId)
				{
					// successfully received an ACK
					request = true;
				}
			}
		}
	}

	return true;
}


void sysInitWaitCAN(const uint16_t canId)
{
	while (true)
	{
		uint8_t dataLen;
		uint8_t data[8];
		uint16_t canIdLatest;

		// read lastest data off CAN bus
		while ((dataLen = canRead(data, &canIdLatest)) > 0)
		{
			if (canId == canIdLatest)
			{
				// latest data has been acquired
				return;
			}
		}

		usleep(10000); // 10 ms
	}
}