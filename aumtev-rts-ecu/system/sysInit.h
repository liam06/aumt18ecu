#ifndef _SYSINIT_H
#define _SYSINIT_H

#include <stdbool.h>
#include <stdint.h>
#include "system/rtTickManager.h"

extern bool sysInit(rtCall *calls, const unsigned int numComponents);

/*
 * Makes sure all ECU nodes are ready for operation.
 * Will wait in function until this is so.
 * Returns false if CAN bus cannot be accessed. 
 * Params:
 *      masterId    ID of the requester (current device)
 *      targetID    ID of the other device
 *      timeout_ms  Number of milli-seconds (0-65535) for timeout.
 */
extern bool sysInitNodeAck(uint8_t masterId, uint8_t targetId, uint16_t timeout_ms);

/*
 * Wait for the given CAN ID to be seen on the CAN bus.
 * Used to wait for other devices (e.g. inverter, BMS) to be ready
 * before continuing.
 * Note: function may sleep the thread.
 */
extern void sysInitWaitCAN(const uint16_t canId);

#endif