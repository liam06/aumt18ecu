#include "tasks.h"

#include <pthread.h>

int taskCreateRR(pthread_t *thread, void *(*taskRoutine)(void*), void* arg, int priority)
{
    struct sched_param param;
    pthread_attr_t attr;

    pthread_attr_init(&attr);
    pthread_attr_setschedpolicy(&attr, SCHED_RR);

    param.sched_priority = priority;
    pthread_attr_setschedparam(&attr, &param);

	int ret = pthread_create(thread, &attr, taskRoutine, arg);
    return ret;
}