#ifndef _RTTICKMANAGER_H
#define _RTTICKMANAGER_H

#include "lib/states.h"

typedef state_t (*functionCall)(void);

typedef struct
{
    functionCall initFunction;
    functionCall tickFunction;
    unsigned int tickDivider;
    unsigned int tickDividerCount;
} rtCall;


void rtTickManagerInit(const unsigned int tickRate, const unsigned long numTicks);	// Initialize the tick manager, tickRate: rate in Hz, numTicks: run this many times, indefinitely if 0
void rtTickManagerRun(rtCall *tickFunctions, const unsigned int numTickFunctions);		// Start running

#endif
