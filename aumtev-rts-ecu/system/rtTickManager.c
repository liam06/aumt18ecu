#include "rtTickManager.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <signal.h>


// rtTickManager Internal data ------------------------------------------------
long delay;    	// Delay between tick updates (nanoseconds)
struct timespec ts;		// Stores the time for the delays

unsigned long numTickCount, numTickMax;

static void sleep_until(struct timespec *ts, long delay);	// Performs a delay

// ----------------------------------------------------------------------------



void rtTickManagerInit(const unsigned int tickRate, const unsigned long numTicks)
{
	printf("LOG: rtTickManager: Initializing tick manager\n");


	delay = 1e9 / tickRate; // Note: Delay in ns
	numTickCount = 0;
	numTickMax = numTicks;


	// signal(SIGINT, dumptimestamps);
	clock_gettime(CLOCK_MONOTONIC, &ts);

	// Lock memory to ensure no swapping is done.
	if(mlockall(MCL_FUTURE|MCL_CURRENT))
	{
		fprintf(stderr,"WARNING: rtTickManager: Failed to lock memory (try running as superuser?)\n");
	}
	else
	{
		printf("LOG: rtTickManager: Successfully locked memory\n");
	}

	// Set our thread to real time priority
	struct sched_param sp;
	sp.sched_priority = 30;
	if(pthread_setschedparam(pthread_self(), SCHED_RR, &sp))
	{
		fprintf(stderr,"WARNING: rtTickManager: Failed to thread to real-time priority (try running as superuser?)\n");
	}
	else
	{
		printf("LOG: rtTickManager: Successfully set thread to real-time priority\n");
	}
}

void rtTickManagerRun(rtCall *tickFunctions, const unsigned int numTickFunctions)
{

	while (1)   // run the timed code forever
	{
		sleep_until(&ts, delay);

		unsigned int i;
		for (i = 0; i < numTickFunctions; ++i)
		{
			// check that a tick function is registered
			if (tickFunctions[i].tickFunction == NULL)
			{
				continue;
			}

			// check if it's time to run this tick function (due to clock divider)
			if (tickFunctions[i].tickDividerCount == 0)
			{
				tickFunctions[i].tickFunction();
			}

			if (++tickFunctions[i].tickDividerCount >= tickFunctions[i].tickDivider)
			{
				tickFunctions[i].tickDividerCount = 0;
			}
		}

		if (numTickMax != 0)
		{
			// check tick limit
			numTickCount++;
			if (numTickCount == numTickMax)
			{
				break;
			}
		}
	}

}

// Adds "delay" nanoseconds to timespecs and sleeps until that time
static void sleep_until(struct timespec *ts, long delay)
{

	ts->tv_nsec += delay;
	if(ts->tv_nsec >= 1e9)
	{
			ts->tv_nsec -= 1e9;
			ts->tv_sec++;
	}
	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, ts,  NULL);
}
