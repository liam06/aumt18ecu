#ifndef _TASKS_H
#define _TASKS_H

#include <pthread.h>

// Creates and runs a new thread with RR scheduling
int taskCreateRR(pthread_t *thread, void *(*taskRoutine)(void*), void* arg, int priority);

#endif